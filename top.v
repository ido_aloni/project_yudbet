`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:11:33 12/26/2017 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
 
		output wire UART_TX,
		input wire UART_RX,
		
		input [5:0] SWITCH,
		output reg [7:0] LED,
		
		input CLK
	
    );

localparam UART_BAUD_RATE = 19200;
localparam UART_TX_GAP= 5200;//CLOCK_HZ/UART_BAUD_RATE; // num of bit time gap between symbols
localparam CLOCK_HZ = 100000000;

assign UART_TX = tx_wire;

wire resetn;

wire clk, uart_clk;

assign clk = CLK;
assign uart_clk = CLK;

localparam baud_rate = 19200;
localparam duration = 10; //ns
reg[63:0] sending_time = (1000000000 * 10 / baud_rate) / duration;
reg [31:0] sending_time_tracker ;

reg [32:0] status;

assign resetn = SWITCH[0];

reg uart_resetn_s1;
reg uart_resetn;
always @(posedge uart_clk, negedge resetn) begin
	if (!resetn) begin
		uart_resetn_s1 <= 1'b0;
		uart_resetn <= 1'b0;
	end
	else begin
		uart_resetn_s1 <= resetn;
		uart_resetn <= uart_resetn_s1;
	end
end 

reg core_reset_s1;
reg core_reset_n;
always @(posedge clk, negedge resetn) begin
	if (!resetn) begin
		core_reset_s1 <= 1'b0;
		core_reset_n <= 1'b0;
	end
	else begin
		core_reset_s1 <= resetn;
		core_reset_n <= core_reset_s1;
	end
end 

//////////////// HEX DISPLAY ////////////////


//////////////// LOGIC ////////////////


wire [15:0] l_data_in;
reg [15:0] l_data_out;
wire l_data_valid, l_read_valid;
wire l_can_write = can_write, l_can_read = can_read;
wire l_output_valid = output_valid;

// feed l_data_out:
reg just_received;

always @(posedge clk, negedge resetn) begin
	if (!resetn) begin
		just_received <= 1'b0;
		l_data_out <= 16'd0;
	end
	else begin
		if (read_valid) begin
			l_data_out <= data_out;
		end
	end
end

wire [7:0] temp_monitor;

always @(*) begin
	LED <= temp_monitor;
end

Logic our_logic(
	.clk(clk),
	.resetn(resetn),
	.byte_in(l_data_out),
	.byte_in_valid(l_read_valid),
	.can_read(l_can_read),
	
	.can_write(l_can_write),
	.byte_out(l_data_in),
	.byte_out_valid(l_data_valid),
	.monitor(temp_monitor)
);

//////////////// UART TOP //////////////////

wire [15:0] data_in = l_data_in;
wire [15:0] data_out;
wire data_valid = l_data_valid, read_valid = l_read_valid;
wire can_write, can_read;
wire output_valid;

reg need_to_write_back;
reg need_to_write_back_latency_counter;
reg [15:0] temp_buffer;


wire [7:0] monitor;

uart_top uart_module(
	.uart_clk(uart_clk), 
	.clk(clk), 
	.rstn(core_reset_n), 
	.uart_resetn(uart_resetn) , 
	.can_write(can_write), 
	.can_read(can_read), 
	.data_in(data_in), 
	.data_out(data_out) , 
	.read_valid(read_valid) , 
	.data_valid(data_valid) , 
	.output_valid(output_valid),
	.Tx(tx_wire), 
	.Rx(UART_RX), 
	.monitor(monitor)
);

endmodule