`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:26:45 01/21/2018 
// Design Name: 
// Module Name:    crc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module crc (
clk     ,
resetn   ,
enable  ,
data_in ,
init ,
mask , 
crc_out 
);

parameter LEN = 16;

//-----------Input Ports---------------
input clk     ;
input resetn   ;
input enable  ;
input data_in ;
input [LEN-1:0] mask;
input init;
//-----------Output Ports---------------
output [LEN-1:0] crc_out;
//------------Internal Variables--------
reg   [LEN-1:0] lfsr;
//-------------Code Start-----------------

reg lfsr_curr_bit;

assign crc_out = lfsr;
// Logic to CRC Calculation
always @ (posedge clk, negedge resetn) begin
    if (!resetn) begin
        lfsr = 0;
        lfsr_curr_bit=0;
    end
	 else if (init) begin
	     lfsr = 0;
        lfsr_curr_bit=0;
	 end
    else begin
        if (enable) begin
            lfsr_curr_bit = lfsr[LEN-1];
            lfsr = lfsr << 1;
            lfsr[0] = data_in;
            if (lfsr_curr_bit == 1'b1)
                lfsr = lfsr ^ mask;
        end
    end
end 

endmodule