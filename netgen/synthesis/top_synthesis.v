////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: top_synthesis.v
// /___/   /\     Timestamp: Tue Dec 26 13:16:24 2017
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -intstyle ise -insert_glbl true -w -dir netgen/synthesis -ofmt verilog -sim top.ngc top_synthesis.v 
// Device	: xc6slx9-2-csg324
// Input file	: top.ngc
// Output file	: C:\Users\User\Documents\fpga projects\project_yudbet\netgen\synthesis\top_synthesis.v
// # of Modules	: 1
// Design Name	: top
// Xilinx        : H:\ise\14.7\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module top (
  UART_RX, CLK, UART_TX, SWITCH, LED
)/* synthesis syn_black_box syn_noprune=1 */;
  input UART_RX;
  input CLK;
  output UART_TX;
  input [5 : 0] SWITCH;
  output [7 : 0] LED;
  
  // synthesis translate_off
  
  wire SWITCH_0_IBUF_0;
  wire UART_RX_IBUF_1;
  wire CLK_BUFGP_2;
  wire \uart_module/output_valid_19 ;
  wire \uart_module/u_uart/tx_out_20 ;
  wire reset_21;
  wire data_valid_22;
  wire uart_resetn_s1_23;
  wire uart_resetn_24;
  wire LED_6_25;
  wire LED_5_26;
  wire LED_4_OBUF_27;
  wire LED_3_OBUF_28;
  wire LED_2_29;
  wire LED_1_30;
  wire LED_0_31;
  wire was_down_32;
  wire read_valid_33;
  wire need_to_write_back_34;
  wire can_write_need_to_write_back_AND_6_o;
  wire UART_TX_GND_1_o_MUX_113_o;
  wire read_valid_PWR_1_o_MUX_118_o;
  wire GND_1_o_GND_1_o_MUX_115_o;
  wire was_down_GND_1_o_MUX_114_o;
  wire \SWITCH<0>_inv ;
  wire _n0057_inv;
  wire reset_inv;
  wire _n0049_inv;
  wire \uart_module/Mcount_time_gap_counter4 ;
  wire \uart_module/Mcount_time_gap_counter3 ;
  wire \uart_module/Mcount_time_gap_counter2 ;
  wire \uart_module/Mcount_time_gap_counter1 ;
  wire \uart_module/Mcount_time_gap_counter ;
  wire \uart_module/_n0113_inv ;
  wire \uart_module/_n0130_inv ;
  wire \uart_module/_n0105_inv ;
  wire \uart_module/_n0124_inv ;
  wire \uart_module/_n0127_inv ;
  wire \uart_module/_n0099_inv ;
  wire \uart_module/_n0088 ;
  wire \uart_module/uart_resetn_received_AND_4_o ;
  wire \uart_module/GND_2_o_GND_2_o_MUX_102_o ;
  wire \uart_module/GND_2_o_had_read_MUX_106_o ;
  wire \uart_module/PWR_2_o_need_to_send_MUX_101_o ;
  wire \uart_module/GND_2_o_read_valid_MUX_109_o ;
  wire \uart_module/GND_2_o_received_MUX_108_o ;
  wire \uart_module/GND_2_o_data_valid_MUX_104_o ;
  wire \uart_module/fifo_rst ;
  wire \uart_module/r_rd_en_105 ;
  wire \uart_module/wr_en_106 ;
  wire \uart_module/had_read_107 ;
  wire \uart_module/transmit_108 ;
  wire \uart_module/need_to_send_130 ;
  wire \uart_module/rd_en ;
  wire \uart_module/r_wr_en_132 ;
  wire \uart_module/r_fifo_read_valid ;
  wire \uart_module/r_empty ;
  wire \uart_module/r_full ;
  wire \uart_module/fifo_read_valid ;
  wire \uart_module/empty ;
  wire \uart_module/full ;
  wire \uart_module/received ;
  wire \uart_module/u_uart/Mmux__n01861221 ;
  wire \uart_module/u_uart/Mmux__n01861242 ;
  wire \uart_module/u_uart/n00662_190 ;
  wire \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ;
  wire \uart_module/u_uart/Mmux__n02591111 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<12> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<11>_200 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<11> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<10>_202 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<10> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<9>_204 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<9> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<8>_206 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<8> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<7>_208 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<7> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<6>_210 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<6> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<5>_212 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<5> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<4>_214 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<4> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<3>_216 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<3> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<2>_218 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<2> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<1>_220 ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<1> ;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_222 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<15>_223 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<15> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<14>_225 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<14> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<13>_227 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<13> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<12>_229 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<12> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<11>_231 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<11> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<10>_233 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<10> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<9>_235 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<9> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<8>_237 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<8> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<7>_239 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<7> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<6>_241 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<6> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<5>_243 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<5> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<4>_245 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<4> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<3>_247 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<3> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<2>_249 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<2> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<1>_251 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<1> ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_253 ;
  wire \uart_module/u_uart/Mcount_tx_bits_remaining3 ;
  wire \uart_module/u_uart/Mcount_tx_bits_remaining2 ;
  wire \uart_module/u_uart/Mcount_tx_bits_remaining1 ;
  wire \uart_module/u_uart/Mcount_tx_bits_remaining ;
  wire \uart_module/u_uart/_n0258_inv ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<0> ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<1> ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<2> ;
  wire \uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ;
  wire \uart_module/u_uart/n0066 ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<0> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<1> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<2> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<3> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<4> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<5> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<6> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<7> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<8> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<9> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<10> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<11> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<12> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<13> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<14> ;
  wire \uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<15> ;
  wire \uart_module/u_uart/n0038 ;
  wire \uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<0> ;
  wire \uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<1> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<0> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<1> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<2> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<3> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<4> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<5> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<6> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<7> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<8> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<9> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<10> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<11> ;
  wire \uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<12> ;
  wire \uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<0> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<1> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<2> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<3> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<4> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<5> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<6> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<9> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<10> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<11> ;
  wire \uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<12> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<0> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<1> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<3> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<4> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<5> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<6> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<7> ;
  wire \uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<8> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<0> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<1> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<2> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<3> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<4> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<5> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<7> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<8> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<9> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<10> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<12> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<13> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<14> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<15> ;
  wire \uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<16> ;
  wire \uart_module/u_uart/rx_clk[16]_reduce_or_9_o ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ;
  wire \uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ;
  wire \uart_module/u_uart/Mmux__n0186112 ;
  wire \uart_module/u_uart/Mmux__n01861121_403 ;
  wire \uart_module/u_uart/Mmux__n01861122_404 ;
  wire \uart_module/u_uart/Mmux__n01861123_405 ;
  wire \uart_module/u_uart/Mmux__n0186110 ;
  wire N4;
  wire \uart_module/u_uart/n00661_408 ;
  wire \uart_module/u_uart/n00664 ;
  wire \uart_module/u_uart/out3_410 ;
  wire \uart_module/u_uart/out31_411 ;
  wire \uart_module/u_uart/out32_412 ;
  wire \uart_module/u_uart/out33_413 ;
  wire \uart_module/u_uart/out34_414 ;
  wire \uart_module/u_uart/out35_415 ;
  wire N8;
  wire N10;
  wire \uart_module/u_uart/out ;
  wire \uart_module/u_uart/out1_419 ;
  wire was_down_glue_set_432;
  wire \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_rt_433 ;
  wire \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_rt_434 ;
  wire N12;
  wire \uart_module/u_uart/rx_data_7_rstpot_436 ;
  wire \uart_module/u_uart/rx_bits_remaining_0_rstpot_437 ;
  wire \uart_module/u_uart/tx_out_rstpot_438 ;
  wire \uart_module/u_uart/_n0245_inv1_rstpot_439 ;
  wire \uart_module/u_uart/rx_bits_remaining_2_dpot_440 ;
  wire \uart_module/u_uart/rx_bits_remaining_3_dpot_441 ;
  wire N14;
  wire N17;
  wire N20;
  wire N23;
  wire N24;
  wire N26;
  wire N28;
  wire N29;
  wire N31;
  wire N32;
  wire N35;
  wire N36;
  wire N38;
  wire N39;
  wire N41;
  wire N42;
  wire N44;
  wire N46;
  wire N48;
  wire N49;
  wire N51;
  wire N52;
  wire N53;
  wire N55;
  wire N57;
  wire \uart_module/u_uart/_n0226_inv1_rstpot_467 ;
  wire \uart_module/u_uart/rx_sample_countdown_1_dpot_468 ;
  wire \uart_module/u_uart/rx_sample_countdown_2_dpot_469 ;
  wire \uart_module/u_uart/_n0206_inv1_rstpot_470 ;
  wire \uart_module/u_uart/rx_samples_0_dpot_471 ;
  wire \uart_module/u_uart/rx_samples_1_dpot_472 ;
  wire \uart_module/u_uart/rx_samples_2_dpot_473 ;
  wire N65;
  wire N67;
  wire N68;
  wire N69;
  wire N71;
  wire N72;
  wire N73;
  wire N77;
  wire \uart_module/u_uart/rx_data_6_rstpot_482 ;
  wire \uart_module/u_uart/rx_data_5_rstpot_483 ;
  wire \uart_module/u_uart/rx_data_4_rstpot_484 ;
  wire \uart_module/u_uart/rx_data_3_rstpot_485 ;
  wire \uart_module/u_uart/rx_data_2_rstpot_486 ;
  wire \uart_module/u_uart/rx_data_1_rstpot_487 ;
  wire \uart_module/u_uart/rx_data_0_rstpot_488 ;
  wire N79;
  wire N81;
  wire N83;
  wire N85;
  wire N87;
  wire N89;
  wire N91;
  wire N93;
  wire N94;
  wire N95;
  wire \uart_module/rd_en_LDC_499 ;
  wire \uart_module/rd_en_C_500 ;
  wire \uart_module/rd_en_P_501 ;
  wire \uart_module/u_uart/out36_502 ;
  wire \uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ;
  wire \uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ;
  wire \uart_module/u_uart/rx_samples_0_dpot1_505 ;
  wire \uart_module/u_uart/rx_samples_1_dpot1_506 ;
  wire \uart_module/u_uart/rx_samples_2_dpot1_507 ;
  wire \uart_module/u_uart/rx_samples_3_dpot1_508 ;
  wire \uart_module/u_uart/rx_sample_countdown_0_dpot1_509 ;
  wire \uart_module/u_uart/rx_sample_countdown_1_dpot1_510 ;
  wire \uart_module/u_uart/rx_sample_countdown_2_dpot1_511 ;
  wire \uart_module/u_uart/rx_sample_countdown_3_dpot1_512 ;
  wire \uart_module/u_uart/rx_bits_remaining_1_dpot1_513 ;
  wire \uart_module/u_uart/rx_bits_remaining_2_dpot1_514 ;
  wire \uart_module/u_uart/rx_bits_remaining_3_dpot1_515 ;
  wire \uart_module/u_uart/out37_516 ;
  wire \uart_module/u_uart/out371 ;
  wire \NLW_uart_module/writing_fifo_dout<15>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<14>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<13>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<12>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<11>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<10>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<9>_UNCONNECTED ;
  wire \NLW_uart_module/writing_fifo_dout<8>_UNCONNECTED ;
  wire [15 : 0] \uart_module/data_out ;
  wire [15 : 0] data_in;
  wire [15 : 0] temp_buffer;
  wire [4 : 0] \uart_module/Mcount_time_gap_counter_lut ;
  wire [3 : 0] \uart_module/Mcount_time_gap_counter_cy ;
  wire [7 : 0] \uart_module/rf_data_in ;
  wire [4 : 0] \uart_module/time_gap_counter ;
  wire [7 : 0] \uart_module/tx_byte ;
  wire [15 : 0] \uart_module/rf_data_out ;
  wire [7 : 0] \uart_module/wf_data_out ;
  wire [7 : 0] \uart_module/u_uart/rx_data ;
  wire [15 : 0] \uart_module/wf_data_in ;
  wire [3 : 0] \uart_module/u_uart/rx_bits_remaining ;
  wire [3 : 0] \uart_module/u_uart/rx_samples ;
  wire [3 : 0] \uart_module/u_uart/rx_sample_countdown ;
  wire [3 : 3] \uart_module/u_uart/_n0209 ;
  wire [3 : 3] \uart_module/u_uart/_n0186 ;
  wire [7 : 0] \uart_module/u_uart/tx_data ;
  wire [3 : 0] \uart_module/u_uart/tx_bits_remaining ;
  wire [2 : 0] \uart_module/u_uart/recv_state ;
  wire [12 : 0] \uart_module/u_uart/tx_clk ;
  wire [15 : 0] \uart_module/u_uart/rx_clk ;
  wire [1 : 0] \uart_module/u_uart/tx_state ;
  wire [7 : 0] \uart_module/u_uart/_n0270 ;
  VCC   XST_VCC (
    .P(LED_4_OBUF_27)
  );
  GND   XST_GND (
    .G(LED_3_OBUF_28)
  );
  FDC   reset (
    .C(CLK_BUFGP_2),
    .CLR(\SWITCH<0>_inv ),
    .D(LED_4_OBUF_27),
    .Q(reset_21)
  );
  FDE   temp_buffer_0 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [0]),
    .Q(temp_buffer[0])
  );
  FDE   temp_buffer_1 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [1]),
    .Q(temp_buffer[1])
  );
  FDE   temp_buffer_2 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [2]),
    .Q(temp_buffer[2])
  );
  FDE   temp_buffer_3 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [3]),
    .Q(temp_buffer[3])
  );
  FDE   temp_buffer_4 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [4]),
    .Q(temp_buffer[4])
  );
  FDE   temp_buffer_5 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [5]),
    .Q(temp_buffer[5])
  );
  FDE   temp_buffer_6 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [6]),
    .Q(temp_buffer[6])
  );
  FDE   temp_buffer_7 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [7]),
    .Q(temp_buffer[7])
  );
  FDE   temp_buffer_8 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [8]),
    .Q(temp_buffer[8])
  );
  FDE   temp_buffer_9 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [9]),
    .Q(temp_buffer[9])
  );
  FDE   temp_buffer_10 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [10]),
    .Q(temp_buffer[10])
  );
  FDE   temp_buffer_11 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [11]),
    .Q(temp_buffer[11])
  );
  FDE   temp_buffer_12 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [12]),
    .Q(temp_buffer[12])
  );
  FDE   temp_buffer_13 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [13]),
    .Q(temp_buffer[13])
  );
  FDE   temp_buffer_14 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [14]),
    .Q(temp_buffer[14])
  );
  FDE   temp_buffer_15 (
    .C(CLK_BUFGP_2),
    .CE(_n0057_inv),
    .D(\uart_module/data_out [15]),
    .Q(temp_buffer[15])
  );
  FDC   uart_resetn_s1 (
    .C(CLK_BUFGP_2),
    .CLR(reset_inv),
    .D(reset_21),
    .Q(uart_resetn_s1_23)
  );
  FDC   uart_resetn (
    .C(CLK_BUFGP_2),
    .CLR(reset_inv),
    .D(uart_resetn_s1_23),
    .Q(uart_resetn_24)
  );
  FDC   data_valid (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/fifo_rst ),
    .D(can_write_need_to_write_back_AND_6_o),
    .Q(data_valid_22)
  );
  FDCE #(
    .INIT ( 1'b0 ))
  read_valid (
    .C(CLK_BUFGP_2),
    .CE(_n0049_inv),
    .CLR(\uart_module/fifo_rst ),
    .D(read_valid_PWR_1_o_MUX_118_o),
    .Q(read_valid_33)
  );
  FDCE   data_in_0 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[0]),
    .Q(data_in[0])
  );
  FDCE   data_in_1 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[1]),
    .Q(data_in[1])
  );
  FDCE   data_in_2 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[2]),
    .Q(data_in[2])
  );
  FDCE   data_in_3 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[3]),
    .Q(data_in[3])
  );
  FDCE   data_in_4 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[4]),
    .Q(data_in[4])
  );
  FDCE   data_in_5 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[5]),
    .Q(data_in[5])
  );
  FDCE   data_in_6 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[6]),
    .Q(data_in[6])
  );
  FDCE   data_in_7 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[7]),
    .Q(data_in[7])
  );
  FDCE   data_in_8 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[8]),
    .Q(data_in[8])
  );
  FDCE   data_in_9 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[9]),
    .Q(data_in[9])
  );
  FDCE   data_in_10 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[10]),
    .Q(data_in[10])
  );
  FDCE   data_in_11 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[11]),
    .Q(data_in[11])
  );
  FDCE   data_in_12 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[12]),
    .Q(data_in[12])
  );
  FDCE   data_in_13 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[13]),
    .Q(data_in[13])
  );
  FDCE   data_in_14 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[14]),
    .Q(data_in[14])
  );
  FDCE   data_in_15 (
    .C(CLK_BUFGP_2),
    .CE(can_write_need_to_write_back_AND_6_o),
    .CLR(\uart_module/fifo_rst ),
    .D(temp_buffer[15]),
    .Q(data_in[15])
  );
  FDCE   need_to_write_back (
    .C(CLK_BUFGP_2),
    .CE(_n0049_inv),
    .CLR(\uart_module/fifo_rst ),
    .D(read_valid_33),
    .Q(need_to_write_back_34)
  );
  FD   LED_0 (
    .C(CLK_BUFGP_2),
    .D(GND_1_o_GND_1_o_MUX_115_o),
    .Q(LED_0_31)
  );
  FD   LED_1 (
    .C(CLK_BUFGP_2),
    .D(was_down_GND_1_o_MUX_114_o),
    .Q(LED_1_30)
  );
  FD   LED_2 (
    .C(CLK_BUFGP_2),
    .D(UART_TX_GND_1_o_MUX_113_o),
    .Q(LED_2_29)
  );
  FD   LED_5 (
    .C(CLK_BUFGP_2),
    .D(reset_21),
    .Q(LED_5_26)
  );
  FD   LED_6 (
    .C(CLK_BUFGP_2),
    .D(uart_resetn_24),
    .Q(LED_6_25)
  );
  FDCE   \uart_module/time_gap_counter_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0113_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/Mcount_time_gap_counter4 ),
    .Q(\uart_module/time_gap_counter [4])
  );
  FDCE   \uart_module/time_gap_counter_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0113_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/Mcount_time_gap_counter3 ),
    .Q(\uart_module/time_gap_counter [3])
  );
  FDCE   \uart_module/time_gap_counter_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0113_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/Mcount_time_gap_counter2 ),
    .Q(\uart_module/time_gap_counter [2])
  );
  FDCE   \uart_module/time_gap_counter_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0113_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/Mcount_time_gap_counter1 ),
    .Q(\uart_module/time_gap_counter [1])
  );
  FDCE   \uart_module/time_gap_counter_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0113_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/Mcount_time_gap_counter ),
    .Q(\uart_module/time_gap_counter [0])
  );
  XORCY   \uart_module/Mcount_time_gap_counter_xor<4>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [3]),
    .LI(\uart_module/Mcount_time_gap_counter_lut [4]),
    .O(\uart_module/Mcount_time_gap_counter4 )
  );
  LUT3 #(
    .INIT ( 8'h1B ))
  \uart_module/Mcount_time_gap_counter_lut<4>  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/time_gap_counter [4]),
    .I2(LED_4_OBUF_27),
    .O(\uart_module/Mcount_time_gap_counter_lut [4])
  );
  XORCY   \uart_module/Mcount_time_gap_counter_xor<3>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [2]),
    .LI(\uart_module/Mcount_time_gap_counter_lut [3]),
    .O(\uart_module/Mcount_time_gap_counter3 )
  );
  MUXCY   \uart_module/Mcount_time_gap_counter_cy<3>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [2]),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/Mcount_time_gap_counter_lut [3]),
    .O(\uart_module/Mcount_time_gap_counter_cy [3])
  );
  LUT3 #(
    .INIT ( 8'h1B ))
  \uart_module/Mcount_time_gap_counter_lut<3>  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/time_gap_counter [3]),
    .I2(LED_3_OBUF_28),
    .O(\uart_module/Mcount_time_gap_counter_lut [3])
  );
  XORCY   \uart_module/Mcount_time_gap_counter_xor<2>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [1]),
    .LI(\uart_module/Mcount_time_gap_counter_lut [2]),
    .O(\uart_module/Mcount_time_gap_counter2 )
  );
  MUXCY   \uart_module/Mcount_time_gap_counter_cy<2>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [1]),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/Mcount_time_gap_counter_lut [2]),
    .O(\uart_module/Mcount_time_gap_counter_cy [2])
  );
  LUT3 #(
    .INIT ( 8'h1B ))
  \uart_module/Mcount_time_gap_counter_lut<2>  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/time_gap_counter [2]),
    .I2(LED_4_OBUF_27),
    .O(\uart_module/Mcount_time_gap_counter_lut [2])
  );
  XORCY   \uart_module/Mcount_time_gap_counter_xor<1>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [0]),
    .LI(\uart_module/Mcount_time_gap_counter_lut [1]),
    .O(\uart_module/Mcount_time_gap_counter1 )
  );
  MUXCY   \uart_module/Mcount_time_gap_counter_cy<1>  (
    .CI(\uart_module/Mcount_time_gap_counter_cy [0]),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/Mcount_time_gap_counter_lut [1]),
    .O(\uart_module/Mcount_time_gap_counter_cy [1])
  );
  LUT3 #(
    .INIT ( 8'h1B ))
  \uart_module/Mcount_time_gap_counter_lut<1>  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/time_gap_counter [1]),
    .I2(LED_3_OBUF_28),
    .O(\uart_module/Mcount_time_gap_counter_lut [1])
  );
  XORCY   \uart_module/Mcount_time_gap_counter_xor<0>  (
    .CI(\uart_module/need_to_send_130 ),
    .LI(\uart_module/Mcount_time_gap_counter_lut [0]),
    .O(\uart_module/Mcount_time_gap_counter )
  );
  MUXCY   \uart_module/Mcount_time_gap_counter_cy<0>  (
    .CI(\uart_module/need_to_send_130 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/Mcount_time_gap_counter_lut [0]),
    .O(\uart_module/Mcount_time_gap_counter_cy [0])
  );
  LUT3 #(
    .INIT ( 8'h1B ))
  \uart_module/Mcount_time_gap_counter_lut<0>  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/time_gap_counter [0]),
    .I2(LED_3_OBUF_28),
    .O(\uart_module/Mcount_time_gap_counter_lut [0])
  );
  FDC   \uart_module/output_valid  (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/r_fifo_read_valid ),
    .Q(\uart_module/output_valid_19 )
  );
  FDE   \uart_module/data_out_15  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [15]),
    .Q(\uart_module/data_out [15])
  );
  FDE   \uart_module/data_out_14  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [14]),
    .Q(\uart_module/data_out [14])
  );
  FDE   \uart_module/data_out_13  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [13]),
    .Q(\uart_module/data_out [13])
  );
  FDE   \uart_module/data_out_12  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [12]),
    .Q(\uart_module/data_out [12])
  );
  FDE   \uart_module/data_out_11  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [11]),
    .Q(\uart_module/data_out [11])
  );
  FDE   \uart_module/data_out_10  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [10]),
    .Q(\uart_module/data_out [10])
  );
  FDE   \uart_module/data_out_9  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [9]),
    .Q(\uart_module/data_out [9])
  );
  FDE   \uart_module/data_out_8  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [8]),
    .Q(\uart_module/data_out [8])
  );
  FDE   \uart_module/data_out_7  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [7]),
    .Q(\uart_module/data_out [7])
  );
  FDE   \uart_module/data_out_6  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [6]),
    .Q(\uart_module/data_out [6])
  );
  FDE   \uart_module/data_out_5  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [5]),
    .Q(\uart_module/data_out [5])
  );
  FDE   \uart_module/data_out_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [4]),
    .Q(\uart_module/data_out [4])
  );
  FDE   \uart_module/data_out_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [3]),
    .Q(\uart_module/data_out [3])
  );
  FDE   \uart_module/data_out_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [2]),
    .Q(\uart_module/data_out [2])
  );
  FDE   \uart_module/data_out_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [1]),
    .Q(\uart_module/data_out [1])
  );
  FDE   \uart_module/data_out_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0130_inv ),
    .D(\uart_module/rf_data_out [0]),
    .Q(\uart_module/data_out [0])
  );
  FDC   \uart_module/r_wr_en  (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/GND_2_o_received_MUX_108_o ),
    .Q(\uart_module/r_wr_en_132 )
  );
  FDCE   \uart_module/rf_data_in_7  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [7]),
    .Q(\uart_module/rf_data_in [7])
  );
  FDCE   \uart_module/rf_data_in_6  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [6]),
    .Q(\uart_module/rf_data_in [6])
  );
  FDCE   \uart_module/rf_data_in_5  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [5]),
    .Q(\uart_module/rf_data_in [5])
  );
  FDCE   \uart_module/rf_data_in_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [4]),
    .Q(\uart_module/rf_data_in [4])
  );
  FDCE   \uart_module/rf_data_in_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [3]),
    .Q(\uart_module/rf_data_in [3])
  );
  FDCE   \uart_module/rf_data_in_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [2]),
    .Q(\uart_module/rf_data_in [2])
  );
  FDCE   \uart_module/rf_data_in_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [1]),
    .Q(\uart_module/rf_data_in [1])
  );
  FDCE   \uart_module/rf_data_in_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_received_MUX_108_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/u_uart/rx_data [0]),
    .Q(\uart_module/rf_data_in [0])
  );
  FDCE   \uart_module/tx_byte_7  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [7]),
    .Q(\uart_module/tx_byte [7])
  );
  FDCE   \uart_module/tx_byte_6  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [6]),
    .Q(\uart_module/tx_byte [6])
  );
  FDCE   \uart_module/tx_byte_5  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [5]),
    .Q(\uart_module/tx_byte [5])
  );
  FDCE   \uart_module/tx_byte_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [4]),
    .Q(\uart_module/tx_byte [4])
  );
  FDCE   \uart_module/tx_byte_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [3]),
    .Q(\uart_module/tx_byte [3])
  );
  FDCE   \uart_module/tx_byte_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [2]),
    .Q(\uart_module/tx_byte [2])
  );
  FDCE   \uart_module/tx_byte_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [1]),
    .Q(\uart_module/tx_byte [1])
  );
  FDCE   \uart_module/tx_byte_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0105_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/wf_data_out [0]),
    .Q(\uart_module/tx_byte [0])
  );
  FDCE   \uart_module/transmit  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0124_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/GND_2_o_GND_2_o_MUX_102_o ),
    .Q(\uart_module/transmit_108 )
  );
  FDC   \uart_module/r_rd_en  (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/GND_2_o_read_valid_MUX_109_o ),
    .Q(\uart_module/r_rd_en_105 )
  );
  FDC   \uart_module/wr_en  (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .Q(\uart_module/wr_en_106 )
  );
  FDCE   \uart_module/had_read  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0127_inv ),
    .CLR(\uart_module/fifo_rst ),
    .D(\uart_module/need_to_send_130 ),
    .Q(\uart_module/had_read_107 )
  );
  FDPE   \uart_module/need_to_send  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/_n0099_inv ),
    .D(\uart_module/PWR_2_o_need_to_send_MUX_101_o ),
    .PRE(\uart_module/fifo_rst ),
    .Q(\uart_module/need_to_send_130 )
  );
  FDCE   \uart_module/wf_data_in_15  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[15]),
    .Q(\uart_module/wf_data_in [15])
  );
  FDCE   \uart_module/wf_data_in_14  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[14]),
    .Q(\uart_module/wf_data_in [14])
  );
  FDCE   \uart_module/wf_data_in_13  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[13]),
    .Q(\uart_module/wf_data_in [13])
  );
  FDCE   \uart_module/wf_data_in_12  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[12]),
    .Q(\uart_module/wf_data_in [12])
  );
  FDCE   \uart_module/wf_data_in_11  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[11]),
    .Q(\uart_module/wf_data_in [11])
  );
  FDCE   \uart_module/wf_data_in_10  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[10]),
    .Q(\uart_module/wf_data_in [10])
  );
  FDCE   \uart_module/wf_data_in_9  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[9]),
    .Q(\uart_module/wf_data_in [9])
  );
  FDCE   \uart_module/wf_data_in_8  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[8]),
    .Q(\uart_module/wf_data_in [8])
  );
  FDCE   \uart_module/wf_data_in_7  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[7]),
    .Q(\uart_module/wf_data_in [7])
  );
  FDCE   \uart_module/wf_data_in_6  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[6]),
    .Q(\uart_module/wf_data_in [6])
  );
  FDCE   \uart_module/wf_data_in_5  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[5]),
    .Q(\uart_module/wf_data_in [5])
  );
  FDCE   \uart_module/wf_data_in_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[4]),
    .Q(\uart_module/wf_data_in [4])
  );
  FDCE   \uart_module/wf_data_in_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[3]),
    .Q(\uart_module/wf_data_in [3])
  );
  FDCE   \uart_module/wf_data_in_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[2]),
    .Q(\uart_module/wf_data_in [2])
  );
  FDCE   \uart_module/wf_data_in_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[1]),
    .Q(\uart_module/wf_data_in [1])
  );
  FDCE   \uart_module/wf_data_in_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/GND_2_o_data_valid_MUX_104_o ),
    .CLR(\uart_module/fifo_rst ),
    .D(data_in[0]),
    .Q(\uart_module/wf_data_in [0])
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<12>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<11>_200 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<12> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<12> )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<11>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<10>_202 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<11> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<11> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<11>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<10>_202 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<11> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<11>_200 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<10>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<9>_204 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<10> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<10> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<10>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<9>_204 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<10> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<10>_202 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<9>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<8>_206 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<9> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<9> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<9>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<8>_206 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<9> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<9>_204 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<8>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<7>_208 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<8> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<8>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<7>_208 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<8> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<8>_206 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<7>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<6>_210 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<7> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<7>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<6>_210 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<7> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<7>_208 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<6>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<5>_212 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<6> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<6> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<6>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<5>_212 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<6> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<6>_210 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<5>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<4>_214 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<5> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<5> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<5>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<4>_214 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<5> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<5>_212 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<4>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<3>_216 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<4> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<4> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<4>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<3>_216 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<4> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<4>_214 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<3>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<2>_218 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<3> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<3> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<3>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<2>_218 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<3> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<3>_216 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<2>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<1>_220 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<2> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<2> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<2>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<1>_220 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<2> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<2>_218 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<1>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_222 ),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<1> ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<1> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<1>  (
    .CI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_222 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<1> ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<1>_220 )
  );
  XORCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_xor<0>  (
    .CI(LED_4_OBUF_27),
    .LI(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_rt_433 ),
    .O(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<0> )
  );
  MUXCY   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>  (
    .CI(LED_4_OBUF_27),
    .DI(LED_3_OBUF_28),
    .S(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_rt_433 ),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_222 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<16>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<15>_223 ),
    .LI(LED_4_OBUF_27),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<16> )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<15>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<14>_225 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<15> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<15> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<15>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<14>_225 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<15> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<15>_223 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<14>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<13>_227 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<14> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<14> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<14>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<13>_227 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<14> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<14>_225 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<13>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<12>_229 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<13> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<13> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<13>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<12>_229 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<13> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<13>_227 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<12>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<11>_231 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<12> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<12> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<12>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<11>_231 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<12> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<12>_229 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<11>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<10>_233 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<11> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<11>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<10>_233 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<11> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<11>_231 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<10>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<9>_235 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<10> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<10> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<10>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<9>_235 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<10> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<10>_233 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<9>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<8>_237 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<9> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<9> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<9>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<8>_237 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<9> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<9>_235 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<8>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<7>_239 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<8> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<8> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<8>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<7>_239 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<8> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<8>_237 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<7>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<6>_241 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<7> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<7> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<7>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<6>_241 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<7> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<7>_239 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<6>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<5>_243 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<6> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<6>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<5>_243 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<6> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<6>_241 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<5>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<4>_245 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<5> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<5> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<5>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<4>_245 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<5> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<5>_243 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<4>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<3>_247 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<4> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<4> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<4>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<3>_247 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<4> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<4>_245 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<3>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<2>_249 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<3> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<3> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<3>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<2>_249 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<3> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<3>_247 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<2>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<1>_251 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<2> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<2> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<2>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<1>_251 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<2> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<2>_249 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<1>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_253 ),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<1> ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<1> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<1>  (
    .CI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_253 ),
    .DI(LED_4_OBUF_27),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<1> ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<1>_251 )
  );
  XORCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_xor<0>  (
    .CI(LED_4_OBUF_27),
    .LI(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_rt_434 ),
    .O(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<0> )
  );
  MUXCY   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>  (
    .CI(LED_4_OBUF_27),
    .DI(LED_3_OBUF_28),
    .S(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_rt_434 ),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_253 )
  );
  FDE   \uart_module/u_uart/tx_bits_remaining_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/Mcount_tx_bits_remaining3 ),
    .Q(\uart_module/u_uart/tx_bits_remaining [3])
  );
  FDE   \uart_module/u_uart/tx_bits_remaining_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/Mcount_tx_bits_remaining2 ),
    .Q(\uart_module/u_uart/tx_bits_remaining [2])
  );
  FDE   \uart_module/u_uart/tx_bits_remaining_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/Mcount_tx_bits_remaining1 ),
    .Q(\uart_module/u_uart/tx_bits_remaining [1])
  );
  FDE   \uart_module/u_uart/tx_bits_remaining_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/Mcount_tx_bits_remaining ),
    .Q(\uart_module/u_uart/tx_bits_remaining [0])
  );
  FDE   \uart_module/u_uart/rx_sample_countdown_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_sample_countdown_3_dpot1_512 ),
    .Q(\uart_module/u_uart/rx_sample_countdown [3])
  );
  FDE   \uart_module/u_uart/rx_sample_countdown_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_sample_countdown_2_dpot1_511 ),
    .Q(\uart_module/u_uart/rx_sample_countdown [2])
  );
  FDE   \uart_module/u_uart/rx_sample_countdown_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_sample_countdown_1_dpot1_510 ),
    .Q(\uart_module/u_uart/rx_sample_countdown [1])
  );
  FDE   \uart_module/u_uart/rx_sample_countdown_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_sample_countdown_0_dpot1_509 ),
    .Q(\uart_module/u_uart/rx_sample_countdown [0])
  );
  FDE   \uart_module/u_uart/rx_samples_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_samples_3_dpot1_508 ),
    .Q(\uart_module/u_uart/rx_samples [3])
  );
  FDE   \uart_module/u_uart/rx_samples_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_samples_2_dpot1_507 ),
    .Q(\uart_module/u_uart/rx_samples [2])
  );
  FDE   \uart_module/u_uart/rx_samples_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_samples_1_dpot1_506 ),
    .Q(\uart_module/u_uart/rx_samples [1])
  );
  FDE   \uart_module/u_uart/rx_samples_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_samples_0_dpot1_505 ),
    .Q(\uart_module/u_uart/rx_samples [0])
  );
  FDE   \uart_module/u_uart/rx_bits_remaining_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_bits_remaining_3_dpot1_515 ),
    .Q(\uart_module/u_uart/rx_bits_remaining [3])
  );
  FDE   \uart_module/u_uart/rx_bits_remaining_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_bits_remaining_2_dpot1_514 ),
    .Q(\uart_module/u_uart/rx_bits_remaining [2])
  );
  FDE   \uart_module/u_uart/rx_bits_remaining_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 ),
    .D(\uart_module/u_uart/rx_bits_remaining_1_dpot1_513 ),
    .Q(\uart_module/u_uart/rx_bits_remaining [1])
  );
  FD   \uart_module/u_uart/tx_clk_12  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<12> ),
    .Q(\uart_module/u_uart/tx_clk [12])
  );
  FD   \uart_module/u_uart/tx_clk_11  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<11> ),
    .Q(\uart_module/u_uart/tx_clk [11])
  );
  FD   \uart_module/u_uart/tx_clk_10  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<10> ),
    .Q(\uart_module/u_uart/tx_clk [10])
  );
  FD   \uart_module/u_uart/tx_clk_9  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<9> ),
    .Q(\uart_module/u_uart/tx_clk [9])
  );
  FD   \uart_module/u_uart/tx_clk_8  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<8> ),
    .Q(\uart_module/u_uart/tx_clk [8])
  );
  FD   \uart_module/u_uart/tx_clk_7  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<7> ),
    .Q(\uart_module/u_uart/tx_clk [7])
  );
  FD   \uart_module/u_uart/tx_clk_6  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<6> ),
    .Q(\uart_module/u_uart/tx_clk [6])
  );
  FD   \uart_module/u_uart/tx_clk_5  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<5> ),
    .Q(\uart_module/u_uart/tx_clk [5])
  );
  FD   \uart_module/u_uart/tx_clk_4  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<4> ),
    .Q(\uart_module/u_uart/tx_clk [4])
  );
  FD   \uart_module/u_uart/tx_clk_3  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<3> ),
    .Q(\uart_module/u_uart/tx_clk [3])
  );
  FD   \uart_module/u_uart/tx_clk_2  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<2> ),
    .Q(\uart_module/u_uart/tx_clk [2])
  );
  FD   \uart_module/u_uart/tx_clk_1  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<1> ),
    .Q(\uart_module/u_uart/tx_clk [1])
  );
  FD   \uart_module/u_uart/tx_clk_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<0> ),
    .Q(\uart_module/u_uart/tx_clk [0])
  );
  FD   \uart_module/u_uart/rx_clk_15  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<15> ),
    .Q(\uart_module/u_uart/rx_clk [15])
  );
  FD   \uart_module/u_uart/rx_clk_14  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<14> ),
    .Q(\uart_module/u_uart/rx_clk [14])
  );
  FD   \uart_module/u_uart/rx_clk_13  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<13> ),
    .Q(\uart_module/u_uart/rx_clk [13])
  );
  FD   \uart_module/u_uart/rx_clk_12  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<12> ),
    .Q(\uart_module/u_uart/rx_clk [12])
  );
  FD   \uart_module/u_uart/rx_clk_11  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<11> ),
    .Q(\uart_module/u_uart/rx_clk [11])
  );
  FD   \uart_module/u_uart/rx_clk_10  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<10> ),
    .Q(\uart_module/u_uart/rx_clk [10])
  );
  FD   \uart_module/u_uart/rx_clk_9  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<9> ),
    .Q(\uart_module/u_uart/rx_clk [9])
  );
  FD   \uart_module/u_uart/rx_clk_8  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<8> ),
    .Q(\uart_module/u_uart/rx_clk [8])
  );
  FD   \uart_module/u_uart/rx_clk_7  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<7> ),
    .Q(\uart_module/u_uart/rx_clk [7])
  );
  FD   \uart_module/u_uart/rx_clk_6  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<6> ),
    .Q(\uart_module/u_uart/rx_clk [6])
  );
  FD   \uart_module/u_uart/rx_clk_5  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<5> ),
    .Q(\uart_module/u_uart/rx_clk [5])
  );
  FD   \uart_module/u_uart/rx_clk_4  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<4> ),
    .Q(\uart_module/u_uart/rx_clk [4])
  );
  FD   \uart_module/u_uart/rx_clk_3  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<3> ),
    .Q(\uart_module/u_uart/rx_clk [3])
  );
  FD   \uart_module/u_uart/rx_clk_2  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<2> ),
    .Q(\uart_module/u_uart/rx_clk [2])
  );
  FD   \uart_module/u_uart/rx_clk_1  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<1> ),
    .Q(\uart_module/u_uart/rx_clk [1])
  );
  FD   \uart_module/u_uart/rx_clk_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<0> ),
    .Q(\uart_module/u_uart/rx_clk [0])
  );
  FD #(
    .INIT ( 1'b0 ))
  \uart_module/u_uart/recv_state_2  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<2> ),
    .Q(\uart_module/u_uart/recv_state [2])
  );
  FD #(
    .INIT ( 1'b0 ))
  \uart_module/u_uart/recv_state_1  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<1> ),
    .Q(\uart_module/u_uart/recv_state [1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \uart_module/u_uart/recv_state_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<0> ),
    .Q(\uart_module/u_uart/recv_state [0])
  );
  FD #(
    .INIT ( 1'b0 ))
  \uart_module/u_uart/tx_state_1  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<1> ),
    .Q(\uart_module/u_uart/tx_state [1])
  );
  FD #(
    .INIT ( 1'b0 ))
  \uart_module/u_uart/tx_state_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<0> ),
    .Q(\uart_module/u_uart/tx_state [0])
  );
  FDE   \uart_module/u_uart/tx_data_7  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [7]),
    .Q(\uart_module/u_uart/tx_data [7])
  );
  FDE   \uart_module/u_uart/tx_data_6  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [6]),
    .Q(\uart_module/u_uart/tx_data [6])
  );
  FDE   \uart_module/u_uart/tx_data_5  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [5]),
    .Q(\uart_module/u_uart/tx_data [5])
  );
  FDE   \uart_module/u_uart/tx_data_4  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [4]),
    .Q(\uart_module/u_uart/tx_data [4])
  );
  FDE   \uart_module/u_uart/tx_data_3  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [3]),
    .Q(\uart_module/u_uart/tx_data [3])
  );
  FDE   \uart_module/u_uart/tx_data_2  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [2]),
    .Q(\uart_module/u_uart/tx_data [2])
  );
  FDE   \uart_module/u_uart/tx_data_1  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [1]),
    .Q(\uart_module/u_uart/tx_data [1])
  );
  FDE   \uart_module/u_uart/tx_data_0  (
    .C(CLK_BUFGP_2),
    .CE(\uart_module/u_uart/_n0258_inv ),
    .D(\uart_module/u_uart/_n0270 [0]),
    .Q(\uart_module/u_uart/tx_data [0])
  );
  LUT2 #(
    .INIT ( 4'h4 ))
  can_write_need_to_write_back_AND_6_o1 (
    .I0(\uart_module/full ),
    .I1(need_to_write_back_34),
    .O(can_write_need_to_write_back_AND_6_o)
  );
  LUT3 #(
    .INIT ( 8'h80 ))
  _n0057_inv1 (
    .I0(uart_resetn_24),
    .I1(read_valid_33),
    .I2(\uart_module/output_valid_19 ),
    .O(_n0057_inv)
  );
  LUT2 #(
    .INIT ( 4'hB ))
  _n0049_inv1 (
    .I0(\uart_module/output_valid_19 ),
    .I1(read_valid_33),
    .O(_n0049_inv)
  );
  LUT2 #(
    .INIT ( 4'h4 ))
  Mmux_GND_1_o_GND_1_o_MUX_115_o11 (
    .I0(\uart_module/u_uart/tx_out_20 ),
    .I1(uart_resetn_24),
    .O(GND_1_o_GND_1_o_MUX_115_o)
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  Mmux_UART_TX_GND_1_o_MUX_113_o11 (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_out_20 ),
    .O(UART_TX_GND_1_o_MUX_113_o)
  );
  LUT3 #(
    .INIT ( 8'h80 ))
  Mmux_was_down_GND_1_o_MUX_114_o11 (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_out_20 ),
    .I2(was_down_32),
    .O(was_down_GND_1_o_MUX_114_o)
  );
  LUT3 #(
    .INIT ( 8'h04 ))
  \uart_module/Mmux_GND_2_o_read_valid_MUX_109_o11  (
    .I0(\uart_module/r_empty ),
    .I1(read_valid_33),
    .I2(\uart_module/r_rd_en_105 ),
    .O(\uart_module/GND_2_o_read_valid_MUX_109_o )
  );
  LUT3 #(
    .INIT ( 8'h04 ))
  \uart_module/Mmux_GND_2_o_had_read_MUX_106_o11  (
    .I0(\uart_module/had_read_107 ),
    .I1(\uart_module/need_to_send_130 ),
    .I2(\uart_module/empty ),
    .O(\uart_module/GND_2_o_had_read_MUX_106_o )
  );
  LUT2 #(
    .INIT ( 4'h4 ))
  \uart_module/GND_2_o_data_valid_MUX_104_o1  (
    .I0(\uart_module/full ),
    .I1(data_valid_22),
    .O(\uart_module/GND_2_o_data_valid_MUX_104_o )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \uart_module/_n0130_inv1  (
    .I0(uart_resetn_24),
    .I1(\uart_module/r_fifo_read_valid ),
    .O(\uart_module/_n0130_inv )
  );
  LUT3 #(
    .INIT ( 8'h1F ))
  \uart_module/_n0127_inv1  (
    .I0(\uart_module/empty ),
    .I1(\uart_module/had_read_107 ),
    .I2(\uart_module/need_to_send_130 ),
    .O(\uart_module/_n0127_inv )
  );
  LUT5 #(
    .INIT ( 32'h00000001 ))
  \uart_module/_n0088<15>1  (
    .I0(\uart_module/time_gap_counter [4]),
    .I1(\uart_module/time_gap_counter [3]),
    .I2(\uart_module/time_gap_counter [2]),
    .I3(\uart_module/time_gap_counter [1]),
    .I4(\uart_module/time_gap_counter [0]),
    .O(\uart_module/_n0088 )
  );
  LUT6 #(
    .INIT ( 64'h00F0F0F0030AF0F0 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT31  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<2> )
  );
  LUT6 #(
    .INIT ( 64'hAAAA2A2BAEAC2223 ))
  \uart_module/u_uart/Mmux__n01861211  (
    .I0(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<5> ),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I3(UART_RX_IBUF_1),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<5> )
  );
  LUT6 #(
    .INIT ( 64'hAAA9FFFFFFFFFFFF ))
  \uart_module/u_uart/Mcount_tx_bits_remaining_xor<3>11  (
    .I0(\uart_module/u_uart/tx_bits_remaining [3]),
    .I1(\uart_module/u_uart/tx_bits_remaining [1]),
    .I2(\uart_module/u_uart/tx_bits_remaining [2]),
    .I3(\uart_module/u_uart/tx_bits_remaining [0]),
    .I4(uart_resetn_24),
    .I5(\uart_module/u_uart/tx_state [0]),
    .O(\uart_module/u_uart/Mcount_tx_bits_remaining3 )
  );
  LUT3 #(
    .INIT ( 8'h2A ))
  \uart_module/u_uart/Mmux__n0259181  (
    .I0(\uart_module/tx_byte [7]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .O(\uart_module/u_uart/_n0270 [7])
  );
  LUT4 #(
    .INIT ( 16'hFFFE ))
  \uart_module/u_uart/out21  (
    .I0(\uart_module/u_uart/tx_bits_remaining [2]),
    .I1(\uart_module/u_uart/tx_bits_remaining [0]),
    .I2(\uart_module/u_uart/tx_bits_remaining [1]),
    .I3(\uart_module/u_uart/tx_bits_remaining [3]),
    .O(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o )
  );
  LUT6 #(
    .INIT ( 64'h00000000CCC90000 ))
  \uart_module/u_uart/Mmux__n0186141  (
    .I0(\uart_module/u_uart/rx_sample_countdown [2]),
    .I1(\uart_module/u_uart/rx_sample_countdown [3]),
    .I2(\uart_module/u_uart/rx_sample_countdown [1]),
    .I3(\uart_module/u_uart/rx_sample_countdown [0]),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I5(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .O(\uart_module/u_uart/_n0209 [3])
  );
  LUT6 #(
    .INIT ( 64'hFFFE00C0CFFE00F0 ))
  \uart_module/u_uart/Mmux__n01861171  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I4(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<1> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<1> )
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n02591110  (
    .I0(\uart_module/tx_byte [0]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [1]),
    .O(\uart_module/u_uart/_n0270 [0])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259121  (
    .I0(\uart_module/tx_byte [1]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [2]),
    .O(\uart_module/u_uart/_n0270 [1])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259131  (
    .I0(\uart_module/tx_byte [2]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [3]),
    .O(\uart_module/u_uart/_n0270 [2])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259141  (
    .I0(\uart_module/tx_byte [3]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [4]),
    .O(\uart_module/u_uart/_n0270 [3])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259151  (
    .I0(\uart_module/tx_byte [4]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [5]),
    .O(\uart_module/u_uart/_n0270 [4])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259161  (
    .I0(\uart_module/tx_byte [5]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [6]),
    .O(\uart_module/u_uart/_n0270 [5])
  );
  LUT4 #(
    .INIT ( 16'hEA2A ))
  \uart_module/u_uart/Mmux__n0259171  (
    .I0(\uart_module/tx_byte [6]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_data [7]),
    .O(\uart_module/u_uart/_n0270 [6])
  );
  LUT5 #(
    .INIT ( 32'hFFFF5404 ))
  \uart_module/u_uart/Mmux__n01861151  (
    .I0(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I1(\uart_module/u_uart/rx_clk [15]),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<15> ),
    .I4(\uart_module/u_uart/Mmux__n01861221 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<15> )
  );
  LUT5 #(
    .INIT ( 32'hFFFF5404 ))
  \uart_module/u_uart/Mmux__n01861131  (
    .I0(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I1(\uart_module/u_uart/rx_clk [13]),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<13> ),
    .I4(\uart_module/u_uart/Mmux__n01861221 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<13> )
  );
  LUT4 #(
    .INIT ( 16'hA280 ))
  \uart_module/u_uart/Mmux__n01861141  (
    .I0(\uart_module/u_uart/Mmux__n01861242 ),
    .I1(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<14> ),
    .I3(\uart_module/u_uart/rx_clk [14]),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<14> )
  );
  LUT4 #(
    .INIT ( 16'h8008 ))
  \uart_module/u_uart/Mcount_tx_bits_remaining_xor<1>11  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/u_uart/tx_bits_remaining [0]),
    .I3(\uart_module/u_uart/tx_bits_remaining [1]),
    .O(\uart_module/u_uart/Mcount_tx_bits_remaining1 )
  );
  LUT5 #(
    .INIT ( 32'h80808008 ))
  \uart_module/u_uart/Mcount_tx_bits_remaining_xor<2>11  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/u_uart/tx_bits_remaining [2]),
    .I3(\uart_module/u_uart/tx_bits_remaining [0]),
    .I4(\uart_module/u_uart/tx_bits_remaining [1]),
    .O(\uart_module/u_uart/Mcount_tx_bits_remaining2 )
  );
  LUT6 #(
    .INIT ( 64'h0000000078F00000 ))
  \uart_module/u_uart/Mmux__n018641  (
    .I0(\uart_module/u_uart/rx_samples [2]),
    .I1(\uart_module/u_uart/rx_samples [1]),
    .I2(\uart_module/u_uart/rx_samples [3]),
    .I3(\uart_module/u_uart/rx_samples [0]),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I5(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .O(\uart_module/u_uart/_n0186 [3])
  );
  LUT3 #(
    .INIT ( 8'hFE ))
  \uart_module/u_uart/out11  (
    .I0(\uart_module/u_uart/tx_clk [6]),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(\uart_module/u_uart/n00662_190 ),
    .O(\uart_module/u_uart/n00664 )
  );
  LUT4 #(
    .INIT ( 16'hFFFB ))
  \uart_module/u_uart/out81  (
    .I0(\uart_module/u_uart/rx_bits_remaining [1]),
    .I1(\uart_module/u_uart/rx_bits_remaining [0]),
    .I2(\uart_module/u_uart/rx_bits_remaining [2]),
    .I3(\uart_module/u_uart/rx_bits_remaining [3]),
    .O(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o )
  );
  LUT3 #(
    .INIT ( 8'h40 ))
  \uart_module/u_uart/Mcount_tx_bits_remaining_xor<0>11  (
    .I0(\uart_module/u_uart/tx_bits_remaining [0]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [0]),
    .O(\uart_module/u_uart/Mcount_tx_bits_remaining )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_mux_6_OUT11  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(uart_resetn_24),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_mux_6_OUT21  (
    .I0(\uart_module/u_uart/recv_state [1]),
    .I1(uart_resetn_24),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_mux_6_OUT31  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT17  (
    .I0(\uart_module/u_uart/rx_clk [0]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<0> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<0> )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT131  (
    .I0(\uart_module/u_uart/rx_clk [5]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<5> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<5> )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT141  (
    .I0(\uart_module/u_uart/rx_clk [6]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<6> )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT161  (
    .I0(\uart_module/u_uart/rx_clk [8]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<8> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<8> )
  );
  LUT3 #(
    .INIT ( 8'h80 ))
  \uart_module/u_uart/received<2>1  (
    .I0(\uart_module/u_uart/recv_state [2]),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(\uart_module/u_uart/recv_state [0]),
    .O(\uart_module/received )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux__n01861122  (
    .I0(\uart_module/u_uart/rx_clk [12]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<12> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/Mmux__n01861121_403 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux__n01861101  (
    .I0(\uart_module/u_uart/rx_clk [10]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<10> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/Mmux__n0186110 )
  );
  LUT4 #(
    .INIT ( 16'hFFFB ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT1_SW0  (
    .I0(\uart_module/u_uart/rx_sample_countdown [2]),
    .I1(\uart_module/u_uart/rx_sample_countdown [0]),
    .I2(\uart_module/u_uart/rx_sample_countdown [1]),
    .I3(\uart_module/u_uart/rx_sample_countdown [3]),
    .O(N4)
  );
  LUT6 #(
    .INIT ( 64'h0FFFF0050000F3A5 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT1  (
    .I0(UART_RX_IBUF_1),
    .I1(N4),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<0> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/n00661  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<2> ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<3> ),
    .I2(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<4> ),
    .I3(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<5> ),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<6> ),
    .I5(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> ),
    .O(\uart_module/u_uart/n00661_408 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFF8 ))
  \uart_module/u_uart/out31  (
    .I0(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<16> ),
    .I1(\uart_module/u_uart/out36_502 ),
    .I2(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<3> ),
    .I3(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<1> ),
    .I4(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<4> ),
    .I5(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> ),
    .O(\uart_module/u_uart/out3_410 )
  );
  LUT3 #(
    .INIT ( 8'hFE ))
  \uart_module/u_uart/out32  (
    .I0(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<9> ),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<10> ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<15> ),
    .O(\uart_module/u_uart/out31_411 )
  );
  LUT4 #(
    .INIT ( 16'hFFFE ))
  \uart_module/u_uart/out33  (
    .I0(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<12> ),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<13> ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<14> ),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> ),
    .O(\uart_module/u_uart/out32_412 )
  );
  LUT5 #(
    .INIT ( 32'hFFFFFFFE ))
  \uart_module/u_uart/out34  (
    .I0(\uart_module/u_uart/rx_clk [12]),
    .I1(\uart_module/u_uart/rx_clk [10]),
    .I2(\uart_module/u_uart/rx_clk [11]),
    .I3(\uart_module/u_uart/rx_clk [14]),
    .I4(\uart_module/u_uart/rx_clk [13]),
    .O(\uart_module/u_uart/out33_413 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFE0 ))
  \uart_module/u_uart/out37  (
    .I0(\uart_module/u_uart/out31_411 ),
    .I1(\uart_module/u_uart/out32_412 ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I3(\uart_module/u_uart/out35_415 ),
    .I4(\uart_module/u_uart/out34_414 ),
    .I5(\uart_module/u_uart/out3_410 ),
    .O(\uart_module/u_uart/n0038 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/n00662_SW0  (
    .I0(\uart_module/u_uart/tx_clk [12]),
    .I1(\uart_module/u_uart/tx_clk [0]),
    .I2(\uart_module/u_uart/tx_clk [11]),
    .I3(\uart_module/u_uart/tx_clk [10]),
    .I4(\uart_module/u_uart/tx_clk [9]),
    .I5(\uart_module/u_uart/tx_clk [8]),
    .O(N10)
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/n00662  (
    .I0(\uart_module/u_uart/tx_clk [1]),
    .I1(\uart_module/u_uart/tx_clk [2]),
    .I2(\uart_module/u_uart/tx_clk [3]),
    .I3(\uart_module/u_uart/tx_clk [4]),
    .I4(\uart_module/u_uart/tx_clk [5]),
    .I5(N10),
    .O(\uart_module/u_uart/n00662_190 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/out1  (
    .I0(\uart_module/u_uart/rx_clk [8]),
    .I1(\uart_module/u_uart/rx_clk [7]),
    .I2(\uart_module/u_uart/rx_clk [10]),
    .I3(\uart_module/u_uart/rx_clk [9]),
    .I4(\uart_module/u_uart/rx_clk [12]),
    .I5(\uart_module/u_uart/rx_clk [11]),
    .O(\uart_module/u_uart/out )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/out2  (
    .I0(\uart_module/u_uart/rx_clk [2]),
    .I1(\uart_module/u_uart/rx_clk [1]),
    .I2(\uart_module/u_uart/rx_clk [4]),
    .I3(\uart_module/u_uart/rx_clk [3]),
    .I4(\uart_module/u_uart/rx_clk [6]),
    .I5(\uart_module/u_uart/rx_clk [5]),
    .O(\uart_module/u_uart/out1_419 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/out3  (
    .I0(\uart_module/u_uart/rx_clk [15]),
    .I1(\uart_module/u_uart/rx_clk [13]),
    .I2(\uart_module/u_uart/rx_clk [14]),
    .I3(\uart_module/u_uart/rx_clk [0]),
    .I4(\uart_module/u_uart/out1_419 ),
    .I5(\uart_module/u_uart/out ),
    .O(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o )
  );
  IBUF   SWITCH_0_IBUF (
    .I(SWITCH[0]),
    .O(SWITCH_0_IBUF_0)
  );
  IBUF   UART_RX_IBUF (
    .I(UART_RX),
    .O(UART_RX_IBUF_1)
  );
  OBUF   LED_7_OBUF (
    .I(LED_6_25),
    .O(LED[7])
  );
  OBUF   LED_6_OBUF (
    .I(LED_6_25),
    .O(LED[6])
  );
  OBUF   LED_5_OBUF (
    .I(LED_5_26),
    .O(LED[5])
  );
  OBUF   LED_4_OBUF (
    .I(LED_4_OBUF_27),
    .O(LED[4])
  );
  OBUF   LED_3_OBUF (
    .I(LED_3_OBUF_28),
    .O(LED[3])
  );
  OBUF   LED_2_OBUF (
    .I(LED_2_29),
    .O(LED[2])
  );
  OBUF   LED_1_OBUF (
    .I(LED_1_30),
    .O(LED[1])
  );
  OBUF   LED_0_OBUF (
    .I(LED_0_31),
    .O(LED[0])
  );
  OBUF   UART_TX_OBUF (
    .I(\uart_module/u_uart/tx_out_20 ),
    .O(UART_TX)
  );
  FDR   was_down (
    .C(CLK_BUFGP_2),
    .D(was_down_glue_set_432),
    .R(\uart_module/fifo_rst ),
    .Q(was_down_32)
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_rt  (
    .I0(\uart_module/u_uart/tx_clk [0]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_cy<0>_rt_433 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_rt  (
    .I0(\uart_module/u_uart/rx_clk [0]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_cy<0>_rt_434 )
  );
  LUT6 #(
    .INIT ( 64'hFF00FF00FFFFFEFE ))
  \uart_module/u_uart/out35  (
    .I0(\uart_module/u_uart/rx_clk [9]),
    .I1(\uart_module/u_uart/rx_clk [15]),
    .I2(\uart_module/u_uart/rx_clk [0]),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<0> ),
    .I4(\uart_module/u_uart/out33_413 ),
    .I5(\uart_module/u_uart/out36_502 ),
    .O(\uart_module/u_uart/out34_414 )
  );
  LUT4 #(
    .INIT ( 16'hFFFE ))
  \uart_module/u_uart/out36_SW0  (
    .I0(\uart_module/u_uart/rx_clk [8]),
    .I1(\uart_module/u_uart/rx_clk [7]),
    .I2(\uart_module/u_uart/rx_clk [6]),
    .I3(\uart_module/u_uart/rx_clk [5]),
    .O(N12)
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFCAAAAAAAA ))
  \uart_module/u_uart/out36  (
    .I0(N12),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<5> ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<7> ),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<8> ),
    .I4(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ),
    .I5(\uart_module/u_uart/out37_516 ),
    .O(\uart_module/u_uart/out35_415 )
  );
  FD   \uart_module/u_uart/rx_data_7  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_7_rstpot_436 ),
    .Q(\uart_module/u_uart/rx_data [7])
  );
  FD   \uart_module/u_uart/rx_bits_remaining_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_bits_remaining_0_rstpot_437 ),
    .Q(\uart_module/u_uart/rx_bits_remaining [0])
  );
  FD #(
    .INIT ( 1'b1 ))
  \uart_module/u_uart/tx_out  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/tx_out_rstpot_438 ),
    .Q(\uart_module/u_uart/tx_out_20 )
  );
  LUT6 #(
    .INIT ( 64'hAAABAAA8FFFFFFFF ))
  \uart_module/u_uart/Mmux__n02591171_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<3> ),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(\uart_module/u_uart/n00662_190 ),
    .I3(\uart_module/u_uart/tx_clk [6]),
    .I4(\uart_module/u_uart/tx_clk [3]),
    .I5(\uart_module/u_uart/Mmux__n02591111 ),
    .O(N14)
  );
  LUT6 #(
    .INIT ( 64'hAAABAAA8FFFFFFFF ))
  \uart_module/u_uart/Mmux__n02591181_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<4> ),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(\uart_module/u_uart/n00662_190 ),
    .I3(\uart_module/u_uart/tx_clk [6]),
    .I4(\uart_module/u_uart/tx_clk [4]),
    .I5(\uart_module/u_uart/Mmux__n02591111 ),
    .O(N17)
  );
  LUT6 #(
    .INIT ( 64'hAAABAAA8FFFFFFFF ))
  \uart_module/u_uart/Mmux__n02591141_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<12> ),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(\uart_module/u_uart/n00662_190 ),
    .I3(\uart_module/u_uart/tx_clk [6]),
    .I4(\uart_module/u_uart/tx_clk [12]),
    .I5(\uart_module/u_uart/Mmux__n02591111 ),
    .O(N20)
  );
  LUT4 #(
    .INIT ( 16'hAAA8 ))
  \uart_module/u_uart/Mmux__n02591201_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<6> ),
    .I1(\uart_module/u_uart/n00662_190 ),
    .I2(\uart_module/u_uart/tx_clk [6]),
    .I3(\uart_module/u_uart/tx_clk [7]),
    .O(N23)
  );
  LUT6 #(
    .INIT ( 64'hEEEEEEEC22222220 ))
  \uart_module/u_uart/Mmux__n02591201_SW1  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<6> ),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/tx_clk [6]),
    .I4(\uart_module/u_uart/n00662_190 ),
    .I5(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .O(N24)
  );
  LUT6 #(
    .INIT ( 64'hFFFF0F0FFFBF4F0F ))
  \uart_module/u_uart/Mmux__n02591201  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/Mmux__n02591111 ),
    .I3(N24),
    .I4(N23),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<6> )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux__n0186125_SW0_SW0  (
    .I0(\uart_module/u_uart/rx_clk [9]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<9> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N26)
  );
  LUT6 #(
    .INIT ( 64'hFFF00000EEF02200 ))
  \uart_module/u_uart/Mmux__n01861103  (
    .I0(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I1(N28),
    .I2(N29),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I4(\uart_module/u_uart/Mmux__n0186110 ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<10> )
  );
  LUT5 #(
    .INIT ( 32'hF000CCAA ))
  \uart_module/u_uart/Mmux__n0186193  (
    .I0(N31),
    .I1(N32),
    .I2(\uart_module/u_uart/Mmux__n01861242 ),
    .I3(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<0> ),
    .I4(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<0> )
  );
  LUT6 #(
    .INIT ( 64'hFBFBEAEAFBEAFBEA ))
  \uart_module/u_uart/Mmux__n01861222  (
    .I0(\uart_module/u_uart/Mmux__n01861221 ),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<6> ),
    .I3(N35),
    .I4(N36),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<6> )
  );
  LUT6 #(
    .INIT ( 64'hFEBAFEBAFFBBEEAA ))
  \uart_module/u_uart/Mmux__n0186118  (
    .I0(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> ),
    .I3(N42),
    .I4(N41),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<2> )
  );
  LUT6 #(
    .INIT ( 64'hF0F00000F2F02200 ))
  \uart_module/u_uart/Mmux__n01861241  (
    .I0(N44),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/Mmux__n01861242 ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I4(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<8> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<8> )
  );
  LUT6 #(
    .INIT ( 64'hFFF00000FF222222 ))
  \uart_module/u_uart/Mmux__n01861125  (
    .I0(\uart_module/u_uart/Mmux__n0186112 ),
    .I1(N46),
    .I2(\uart_module/u_uart/Mmux__n01861123_405 ),
    .I3(\uart_module/u_uart/Mmux__n01861122_404 ),
    .I4(\uart_module/u_uart/Mmux__n01861121_403 ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<12> )
  );
  LUT4 #(
    .INIT ( 16'hFCFA ))
  \uart_module/u_uart/Mmux__n0186119_SW2  (
    .I0(\uart_module/u_uart/rx_clk [3]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<3> ),
    .I2(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I3(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N49)
  );
  LUT6 #(
    .INIT ( 64'hFFF30C00FFD33D11 ))
  \uart_module/u_uart/Mmux__n0186119  (
    .I0(N8),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I3(N48),
    .I4(N49),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<3> )
  );
  LUT6 #(
    .INIT ( 64'h33F0F0F033AAAAAA ))
  \uart_module/u_uart/Mmux__n01861112_SW0  (
    .I0(\uart_module/u_uart/rx_clk [11]),
    .I1(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N51)
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/Mmux__n01861112_SW2  (
    .I0(\uart_module/u_uart/rx_clk [11]),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N53)
  );
  LUT6 #(
    .INIT ( 64'hFEBAFEBAFFBBEEAA ))
  \uart_module/u_uart/Mmux__n01861112  (
    .I0(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(N53),
    .I3(N52),
    .I4(N51),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<11> )
  );
  LUT6 #(
    .INIT ( 64'hCCCCCCCCC3C8CCCC ))
  \uart_module/u_uart/rx_bits_remaining_0_rstpot  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/rx_bits_remaining [0]),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/n0038 ),
    .O(\uart_module/u_uart/rx_bits_remaining_0_rstpot_437 )
  );
  LUT4 #(
    .INIT ( 16'hFDA8 ))
  \uart_module/u_uart/_n0183_inv1_SW0  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_samples [3]),
    .I2(\uart_module/u_uart/rx_samples [2]),
    .I3(\uart_module/u_uart/rx_data [7]),
    .O(N55)
  );
  LUT3 #(
    .INIT ( 8'hFE ))
  \uart_module/u_uart/out3_SW0  (
    .I0(\uart_module/u_uart/rx_clk [15]),
    .I1(\uart_module/u_uart/rx_clk [14]),
    .I2(\uart_module/u_uart/rx_clk [13]),
    .O(N57)
  );
  LUT6 #(
    .INIT ( 64'hFF00FF00FF00FE02 ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT91  (
    .I0(\uart_module/u_uart/rx_clk [1]),
    .I1(\uart_module/u_uart/rx_clk [0]),
    .I2(N57),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<1> ),
    .I4(\uart_module/u_uart/out1_419 ),
    .I5(\uart_module/u_uart/out ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<1> )
  );
  LUT6 #(
    .INIT ( 64'hFFFF0000FFFE0002 ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT101  (
    .I0(\uart_module/u_uart/rx_clk [2]),
    .I1(\uart_module/u_uart/rx_clk [0]),
    .I2(N57),
    .I3(\uart_module/u_uart/out1_419 ),
    .I4(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<2> ),
    .I5(\uart_module/u_uart/out ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFE00000002 ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT111  (
    .I0(\uart_module/u_uart/rx_clk [3]),
    .I1(\uart_module/u_uart/rx_clk [0]),
    .I2(N57),
    .I3(\uart_module/u_uart/out1_419 ),
    .I4(\uart_module/u_uart/out ),
    .I5(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<3> ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<3> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFE00000002 ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT121  (
    .I0(\uart_module/u_uart/rx_clk [4]),
    .I1(\uart_module/u_uart/rx_clk [0]),
    .I2(N57),
    .I3(\uart_module/u_uart/out1_419 ),
    .I4(\uart_module/u_uart/out ),
    .I5(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<4> ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<4> )
  );
  LUT6 #(
    .INIT ( 64'h8C008C008C00BC00 ))
  \uart_module/u_uart/Mmux__n02591101  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<1> )
  );
  LUT6 #(
    .INIT ( 64'h0FCFFAFA00C5F0F0 ))
  \uart_module/u_uart/Mmux__n01861234_SW0  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<7> ),
    .O(N38)
  );
  LUT5 #(
    .INIT ( 32'hAAABAAA8 ))
  \uart_module/u_uart/Mmux__n02591121_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<10> ),
    .I1(\uart_module/u_uart/n00662_190 ),
    .I2(\uart_module/u_uart/tx_clk [6]),
    .I3(\uart_module/u_uart/tx_clk [7]),
    .I4(\uart_module/u_uart/tx_clk [10]),
    .O(N65)
  );
  LUT6 #(
    .INIT ( 64'hFA0AFF0FFB0BFB0B ))
  \uart_module/u_uart/Mmux__n02591211_SW1  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> ),
    .I5(\uart_module/u_uart/n00664 ),
    .O(N68)
  );
  LUT6 #(
    .INIT ( 64'hE0F1A0B1E0F1E4F5 ))
  \uart_module/u_uart/Mmux__n02591211  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(N69),
    .I3(N67),
    .I4(\uart_module/u_uart/n0066 ),
    .I5(N68),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<7> )
  );
  LUT6 #(
    .INIT ( 64'hFC0CFF0FFD0DFD0D ))
  \uart_module/u_uart/Mmux__n02591222_SW1  (
    .I0(\uart_module/u_uart/tx_clk [8]),
    .I1(\uart_module/transmit_108 ),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> ),
    .I5(\uart_module/u_uart/n00664 ),
    .O(N72)
  );
  LUT5 #(
    .INIT ( 32'hF300A2A2 ))
  \uart_module/u_uart/Mmux__n02591222_SW2  (
    .I0(\uart_module/u_uart/tx_clk [8]),
    .I1(\uart_module/transmit_108 ),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> ),
    .I4(\uart_module/u_uart/n00664 ),
    .O(N73)
  );
  LUT6 #(
    .INIT ( 64'hEFABEFEF01010145 ))
  \uart_module/u_uart/Mmux__n02591222  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(N71),
    .I3(\uart_module/u_uart/n0066 ),
    .I4(N72),
    .I5(N73),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<8> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFE00000002 ))
  \uart_module/u_uart/Mmux_rx_clk[16]_rx_clk[16]_mux_10_OUT151  (
    .I0(\uart_module/u_uart/rx_clk [7]),
    .I1(\uart_module/u_uart/rx_clk [0]),
    .I2(N57),
    .I3(\uart_module/u_uart/out1_419 ),
    .I4(\uart_module/u_uart/out ),
    .I5(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<7> ),
    .O(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<7> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/n006661_SW0  (
    .I0(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<0> ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<1> ),
    .I2(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<9> ),
    .I3(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<10> ),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<11> ),
    .I5(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> ),
    .O(N77)
  );
  LUT6 #(
    .INIT ( 64'hFFEEFFEEFFEEF0E0 ))
  \uart_module/u_uart/n006661  (
    .I0(\uart_module/u_uart/tx_clk [6]),
    .I1(\uart_module/u_uart/tx_clk [7]),
    .I2(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<12> ),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/n00661_408 ),
    .I5(N77),
    .O(\uart_module/u_uart/n0066 )
  );
  FD   \uart_module/u_uart/rx_data_6  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_6_rstpot_482 ),
    .Q(\uart_module/u_uart/rx_data [6])
  );
  FD   \uart_module/u_uart/rx_data_5  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_5_rstpot_483 ),
    .Q(\uart_module/u_uart/rx_data [5])
  );
  FD   \uart_module/u_uart/rx_data_4  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_4_rstpot_484 ),
    .Q(\uart_module/u_uart/rx_data [4])
  );
  FD   \uart_module/u_uart/rx_data_3  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_3_rstpot_485 ),
    .Q(\uart_module/u_uart/rx_data [3])
  );
  FD   \uart_module/u_uart/rx_data_2  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_2_rstpot_486 ),
    .Q(\uart_module/u_uart/rx_data [2])
  );
  FD   \uart_module/u_uart/rx_data_1  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_1_rstpot_487 ),
    .Q(\uart_module/u_uart/rx_data [1])
  );
  FD   \uart_module/u_uart/rx_data_0  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/u_uart/rx_data_0_rstpot_488 ),
    .Q(\uart_module/u_uart/rx_data [0])
  );
  LUT6 #(
    .INIT ( 64'hFFFF00C1FFFF30F1 ))
  \uart_module/u_uart/Mmux__n0186125  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<2> ),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I4(N26),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<9> )
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW1  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [6]),
    .I2(\uart_module/u_uart/rx_data [7]),
    .O(N79)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW2  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [5]),
    .I2(\uart_module/u_uart/rx_data [6]),
    .O(N81)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW3  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [4]),
    .I2(\uart_module/u_uart/rx_data [5]),
    .O(N83)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW4  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [3]),
    .I2(\uart_module/u_uart/rx_data [4]),
    .O(N85)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW5  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [2]),
    .I2(\uart_module/u_uart/rx_data [3]),
    .O(N87)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW6  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [1]),
    .I2(\uart_module/u_uart/rx_data [2]),
    .O(N89)
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/u_uart/_n0183_inv1_SW7  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/rx_data [0]),
    .I2(\uart_module/u_uart/rx_data [1]),
    .O(N91)
  );
  LUT5 #(
    .INIT ( 32'hA820FF20 ))
  \uart_module/u_uart/_n0269_inv1_SW0  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/u_uart/tx_out_20 ),
    .I4(\uart_module/transmit_108 ),
    .O(N93)
  );
  LUT5 #(
    .INIT ( 32'hAA220222 ))
  \uart_module/u_uart/_n0269_inv1_SW1  (
    .I0(\uart_module/u_uart/tx_out_20 ),
    .I1(\uart_module/transmit_108 ),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/tx_state [1]),
    .O(N94)
  );
  LUT5 #(
    .INIT ( 32'h8880AAAA ))
  \uart_module/u_uart/_n0269_inv1_SW2  (
    .I0(\uart_module/u_uart/tx_out_20 ),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/tx_state [1]),
    .I3(\uart_module/u_uart/tx_state [0]),
    .I4(\uart_module/transmit_108 ),
    .O(N95)
  );
  LUT6 #(
    .INIT ( 64'hFF00FF00FBFB4040 ))
  \uart_module/u_uart/tx_out_rstpot  (
    .I0(\uart_module/u_uart/tx_data [0]),
    .I1(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I2(N94),
    .I3(N95),
    .I4(N93),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_out_rstpot_438 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_7_rstpot  (
    .I0(\uart_module/u_uart/rx_data [7]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N55),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_7_rstpot_436 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_6_rstpot  (
    .I0(\uart_module/u_uart/rx_data [6]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N79),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_6_rstpot_482 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_5_rstpot  (
    .I0(\uart_module/u_uart/rx_data [5]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N81),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_5_rstpot_483 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_4_rstpot  (
    .I0(\uart_module/u_uart/rx_data [4]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N83),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_4_rstpot_484 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_3_rstpot  (
    .I0(\uart_module/u_uart/rx_data [3]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N85),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_3_rstpot_485 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_2_rstpot  (
    .I0(\uart_module/u_uart/rx_data [2]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N87),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_2_rstpot_486 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_1_rstpot  (
    .I0(\uart_module/u_uart/rx_data [1]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N89),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_1_rstpot_487 )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAABAAA8AAA ))
  \uart_module/u_uart/rx_data_0_rstpot  (
    .I0(\uart_module/u_uart/rx_data [0]),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(N91),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/rx_data_0_rstpot_488 )
  );
  LUT6 #(
    .INIT ( 64'hFFFF8080FF80FF80 ))
  \uart_module/u_uart/Mmux__n01861234  (
    .I0(\uart_module/u_uart/recv_state [2]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<7> ),
    .I3(N38),
    .I4(N39),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<7> )
  );
  LUT6 #(
    .INIT ( 64'h5000500056001600 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT21  (
    .I0(\uart_module/u_uart/recv_state [2]),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I5(\uart_module/u_uart/out371 ),
    .O(\uart_module/u_uart/recv_state[2]_GND_3_o_wide_mux_54_OUT<1> )
  );
  LDC   \uart_module/rd_en_LDC  (
    .CLR(\uart_module/uart_resetn_received_AND_4_o ),
    .D(LED_4_OBUF_27),
    .G(\uart_module/received ),
    .Q(\uart_module/rd_en_LDC_499 )
  );
  FDC   \uart_module/rd_en_C  (
    .C(CLK_BUFGP_2),
    .CLR(\uart_module/uart_resetn_received_AND_4_o ),
    .D(\uart_module/GND_2_o_had_read_MUX_106_o ),
    .Q(\uart_module/rd_en_C_500 )
  );
  FDP   \uart_module/rd_en_P  (
    .C(CLK_BUFGP_2),
    .D(\uart_module/GND_2_o_had_read_MUX_106_o ),
    .PRE(\uart_module/received ),
    .Q(\uart_module/rd_en_P_501 )
  );
  LUT3 #(
    .INIT ( 8'hE4 ))
  \uart_module/rd_en1  (
    .I0(\uart_module/rd_en_LDC_499 ),
    .I1(\uart_module/rd_en_C_500 ),
    .I2(\uart_module/rd_en_P_501 ),
    .O(\uart_module/rd_en )
  );
  LUT5 #(
    .INIT ( 32'hAAFFABFF ))
  \uart_module/u_uart/Mmux__n02591211_SW0  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_clk [6]),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> ),
    .I4(\uart_module/u_uart/n00662_190 ),
    .O(N67)
  );
  LUT6 #(
    .INIT ( 64'hFF550000FC540000 ))
  \uart_module/u_uart/Mmux__n02591211_SW2  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_clk [6]),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<7> ),
    .I5(\uart_module/u_uart/n00662_190 ),
    .O(N69)
  );
  LUT6 #(
    .INIT ( 64'h00F500A000DD0088 ))
  \uart_module/u_uart/Mmux__n01861222_SW0  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/rx_clk [6]),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N35)
  );
  LUT6 #(
    .INIT ( 64'hF0F0F0A0CCCCCC88 ))
  \uart_module/u_uart/Mmux__n01861222_SW1  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/rx_clk [6]),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N36)
  );
  LUT6 #(
    .INIT ( 64'hCCCCFFFFCCCDFFFD ))
  \uart_module/u_uart/Mmux__n02591222_SW0  (
    .I0(\uart_module/u_uart/tx_clk [8]),
    .I1(\uart_module/transmit_108 ),
    .I2(\uart_module/u_uart/tx_clk [6]),
    .I3(\uart_module/u_uart/tx_clk [7]),
    .I4(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<8> ),
    .I5(\uart_module/u_uart/n00662_190 ),
    .O(N71)
  );
  LUT4 #(
    .INIT ( 16'hBF00 ))
  \uart_module/u_uart/Mmux__n0186118_SW2  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> ),
    .O(N42)
  );
  LUT6 #(
    .INIT ( 64'hFFFAAAAA0C000000 ))
  \uart_module/u_uart/Mmux__n01861234_SW1  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [0]),
    .I3(\uart_module/u_uart/recv_state [1]),
    .I4(uart_resetn_24),
    .I5(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<7> ),
    .O(N39)
  );
  LUT6 #(
    .INIT ( 64'h3FFFFFFF0555C555 ))
  \uart_module/u_uart/Mmux__n0186118_SW1  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I5(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<2> ),
    .O(N41)
  );
  LUT2 #(
    .INIT ( 4'hB ))
  was_down_glue_set (
    .I0(was_down_32),
    .I1(\uart_module/u_uart/tx_out_20 ),
    .O(was_down_glue_set_432)
  );
  LUT6 #(
    .INIT ( 64'hAAA9AAAAFFFFAAAA ))
  \uart_module/u_uart/rx_bits_remaining_3_dpot  (
    .I0(\uart_module/u_uart/rx_bits_remaining [3]),
    .I1(\uart_module/u_uart/rx_bits_remaining [2]),
    .I2(\uart_module/u_uart/rx_bits_remaining [0]),
    .I3(\uart_module/u_uart/rx_bits_remaining [1]),
    .I4(\uart_module/u_uart/_n0245_inv1_rstpot_439 ),
    .I5(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .O(\uart_module/u_uart/rx_bits_remaining_3_dpot_441 )
  );
  LUT6 #(
    .INIT ( 64'hFAFAFAFAF2F2F27A ))
  \uart_module/u_uart/rx_sample_countdown_2_dpot  (
    .I0(\uart_module/u_uart/_n0226_inv1_rstpot_467 ),
    .I1(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I2(\uart_module/u_uart/rx_sample_countdown [2]),
    .I3(\uart_module/u_uart/rx_sample_countdown [0]),
    .I4(\uart_module/u_uart/rx_sample_countdown [1]),
    .I5(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .O(\uart_module/u_uart/rx_sample_countdown_2_dpot_469 )
  );
  LUT6 #(
    .INIT ( 64'h46444C444C444C44 ))
  \uart_module/u_uart/rx_samples_2_dpot  (
    .I0(\uart_module/u_uart/_n0206_inv1_rstpot_470 ),
    .I1(\uart_module/u_uart/rx_samples [2]),
    .I2(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<0> ),
    .I3(\uart_module/u_uart/recv_state[2]_GND_3_o_mux_6_OUT<1> ),
    .I4(\uart_module/u_uart/rx_samples [0]),
    .I5(\uart_module/u_uart/rx_samples [1]),
    .O(\uart_module/u_uart/rx_samples_2_dpot_473 )
  );
  LUT5 #(
    .INIT ( 32'h11111115 ))
  \uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT111  (
    .I0(UART_RX_IBUF_1),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [0]),
    .I3(\uart_module/u_uart/recv_state [1]),
    .I4(\uart_module/u_uart/recv_state [2]),
    .O(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 )
  );
  LUT5 #(
    .INIT ( 32'hDFFF8AA8 ))
  \uart_module/u_uart/Mmux__n018612421  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [2]),
    .I4(UART_RX_IBUF_1),
    .O(\uart_module/u_uart/Mmux__n01861242 )
  );
  LUT4 #(
    .INIT ( 16'h4000 ))
  \uart_module/u_uart/Mmux__n018612211  (
    .I0(\uart_module/u_uart/recv_state [0]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [2]),
    .O(\uart_module/u_uart/Mmux__n01861221 )
  );
  LUT5 #(
    .INIT ( 32'hDF5F8808 ))
  \uart_module/u_uart/Mmux__n01861123  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(UART_RX_IBUF_1),
    .O(\uart_module/u_uart/Mmux__n01861122_404 )
  );
  LUT4 #(
    .INIT ( 16'hAA08 ))
  \uart_module/u_uart/Mmux__n01861124  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(\uart_module/u_uart/recv_state [2]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .O(\uart_module/u_uart/Mmux__n01861123_405 )
  );
  LUT5 #(
    .INIT ( 32'h40400040 ))
  \uart_module/u_uart/_n0245_inv1_rstpot  (
    .I0(\uart_module/u_uart/recv_state [2]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [0]),
    .I3(UART_RX_IBUF_1),
    .I4(\uart_module/u_uart/recv_state [1]),
    .O(\uart_module/u_uart/_n0245_inv1_rstpot_439 )
  );
  LUT3 #(
    .INIT ( 8'hF7 ))
  \uart_module/u_uart/Mmux__n01861103_SW0  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(\uart_module/u_uart/recv_state [2]),
    .O(N28)
  );
  LUT4 #(
    .INIT ( 16'h7F28 ))
  \uart_module/u_uart/Mmux__n01861103_SW1  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(\uart_module/u_uart/recv_state [2]),
    .I3(UART_RX_IBUF_1),
    .O(N29)
  );
  LUT3 #(
    .INIT ( 8'hF7 ))
  \uart_module/u_uart/Mmux__n018612121_SW1  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [2]),
    .O(N46)
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591112  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<0> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [0]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<0> )
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591131  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<11> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [11]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<11> )
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591151  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<1> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [1]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<1> )
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591161  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<2> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [2]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<2> )
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591191  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<5> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [5]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<5> )
  );
  LUT6 #(
    .INIT ( 64'h8888888A88888880 ))
  \uart_module/u_uart/Mmux__n02591231  (
    .I0(\uart_module/u_uart/Mmux__n02591111 ),
    .I1(\uart_module/u_uart/tx_clk[12]_GND_3_o_sub_13_OUT<9> ),
    .I2(\uart_module/u_uart/tx_clk [7]),
    .I3(\uart_module/u_uart/n00662_190 ),
    .I4(\uart_module/u_uart/tx_clk [6]),
    .I5(\uart_module/u_uart/tx_clk [9]),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<9> )
  );
  LUT5 #(
    .INIT ( 32'h20222020 ))
  \uart_module/u_uart/_n0226_inv1_rstpot  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(UART_RX_IBUF_1),
    .I4(\uart_module/u_uart/recv_state [0]),
    .O(\uart_module/u_uart/_n0226_inv1_rstpot_467 )
  );
  LUT5 #(
    .INIT ( 32'h22200200 ))
  \uart_module/u_uart/_n0206_inv1_rstpot  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(UART_RX_IBUF_1),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(\uart_module/u_uart/recv_state [1]),
    .O(\uart_module/u_uart/_n0206_inv1_rstpot_470 )
  );
  LUT3 #(
    .INIT ( 8'h15 ))
  \uart_module/u_uart/Mmux__n01861121  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(uart_resetn_24),
    .O(\uart_module/u_uart/Mmux__n0186112 )
  );
  LUT4 #(
    .INIT ( 16'h1555 ))
  \uart_module/uart_resetn_received_AND_4_o1  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .O(\uart_module/uart_resetn_received_AND_4_o )
  );
  LUT4 #(
    .INIT ( 16'h0200 ))
  \uart_module/_n0105_inv1  (
    .I0(\uart_module/need_to_send_130 ),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/fifo_read_valid ),
    .O(\uart_module/_n0105_inv )
  );
  LUT4 #(
    .INIT ( 16'h4000 ))
  \uart_module/GND_2_o_received_MUX_108_o1  (
    .I0(\uart_module/r_full ),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .O(\uart_module/GND_2_o_received_MUX_108_o )
  );
  LUT4 #(
    .INIT ( 16'hFFFB ))
  \uart_module/_n0124_inv1  (
    .I0(\uart_module/u_uart/tx_state [0]),
    .I1(\uart_module/need_to_send_130 ),
    .I2(\uart_module/u_uart/tx_state [1]),
    .I3(\uart_module/fifo_read_valid ),
    .O(\uart_module/_n0124_inv )
  );
  LUT3 #(
    .INIT ( 8'h04 ))
  \uart_module/Mmux_GND_2_o_GND_2_o_MUX_102_o11  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/need_to_send_130 ),
    .I2(\uart_module/u_uart/tx_state [0]),
    .O(\uart_module/GND_2_o_GND_2_o_MUX_102_o )
  );
  LUT6 #(
    .INIT ( 64'hFFFF3F00FFFF2A2A ))
  \uart_module/u_uart/Mmux__n0186119_SW1  (
    .I0(\uart_module/u_uart/rx_clk [3]),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<3> ),
    .I4(\uart_module/u_uart/Mmux_recv_state[2]_GND_3_o_wide_mux_54_OUT11 ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N48)
  );
  LUT4 #(
    .INIT ( 16'hD515 ))
  \uart_module/u_uart/Mmux__n018612121_SW0  (
    .I0(UART_RX_IBUF_1),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .O(N44)
  );
  LUT5 #(
    .INIT ( 32'h11011000 ))
  \uart_module/_n0099_inv1  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/need_to_send_130 ),
    .I3(\uart_module/fifo_read_valid ),
    .I4(\uart_module/_n0088 ),
    .O(\uart_module/_n0099_inv )
  );
  LUT5 #(
    .INIT ( 32'h10001101 ))
  \uart_module/_n0113_inv1  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/need_to_send_130 ),
    .I3(\uart_module/fifo_read_valid ),
    .I4(\uart_module/_n0088 ),
    .O(\uart_module/_n0113_inv )
  );
  LUT4 #(
    .INIT ( 16'hA8FF ))
  \uart_module/u_uart/Mmux__n025911111  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(\uart_module/transmit_108 ),
    .O(\uart_module/u_uart/Mmux__n02591111 )
  );
  LUT6 #(
    .INIT ( 64'hAAAA2AAA88880880 ))
  \uart_module/u_uart/Mmux__n01861201  (
    .I0(\uart_module/u_uart/rx_clk[16]_rx_clk[16]_mux_10_OUT<4> ),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [2]),
    .I4(\uart_module/u_uart/recv_state [0]),
    .I5(UART_RX_IBUF_1),
    .O(\uart_module/u_uart/recv_state[2]_rx_clk[16]_wide_mux_55_OUT<4> )
  );
  LUT6 #(
    .INIT ( 64'hAAAAAAAA12AAA2AA ))
  \uart_module/u_uart/rx_samples_0_dpot  (
    .I0(\uart_module/u_uart/rx_samples [0]),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(UART_RX_IBUF_1),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/recv_state [1]),
    .I5(\uart_module/u_uart/recv_state [2]),
    .O(\uart_module/u_uart/rx_samples_0_dpot_471 )
  );
  LUT6 #(
    .INIT ( 64'hCFFF00008AAA8AAA ))
  \uart_module/u_uart/Mmux__n01861112_SW1  (
    .I0(\uart_module/u_uart/rx_clk [11]),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<11> ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(N52)
  );
  LUT5 #(
    .INIT ( 32'hEA2A2A2A ))
  \uart_module/u_uart/Mmux__n0186119_SW0  (
    .I0(UART_RX_IBUF_1),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I4(\uart_module/u_uart/recv_state [0]),
    .O(N8)
  );
  LUT6 #(
    .INIT ( 64'h80808040AAAAAAAA ))
  \uart_module/u_uart/rx_bits_remaining_2_dpot  (
    .I0(\uart_module/u_uart/rx_bits_remaining [2]),
    .I1(uart_resetn_24),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/rx_bits_remaining [1]),
    .I4(\uart_module/u_uart/rx_bits_remaining [0]),
    .I5(\uart_module/u_uart/_n0245_inv1_rstpot_439 ),
    .O(\uart_module/u_uart/rx_bits_remaining_2_dpot_440 )
  );
  LUT6 #(
    .INIT ( 64'h2020002022200220 ))
  \uart_module/u_uart/Mmux__n0186193_SW0  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [2]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I5(UART_RX_IBUF_1),
    .O(N31)
  );
  LUT6 #(
    .INIT ( 64'hFF7FF77FAA28A228 ))
  \uart_module/u_uart/Mmux__n0186193_SW1  (
    .I0(uart_resetn_24),
    .I1(\uart_module/u_uart/recv_state [1]),
    .I2(\uart_module/u_uart/recv_state [2]),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(\uart_module/u_uart/rx_bits_remaining[3]_reduce_or_41_o ),
    .I5(UART_RX_IBUF_1),
    .O(N32)
  );
  LUT6 #(
    .INIT ( 64'h44C4444444644444 ))
  \uart_module/u_uart/rx_sample_countdown_1_dpot  (
    .I0(\uart_module/u_uart/_n0226_inv1_rstpot_467 ),
    .I1(\uart_module/u_uart/rx_sample_countdown [1]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(\uart_module/u_uart/recv_state [1]),
    .I5(\uart_module/u_uart/rx_sample_countdown [0]),
    .O(\uart_module/u_uart/rx_sample_countdown_1_dpot_468 )
  );
  LUT6 #(
    .INIT ( 64'h4464444444C44444 ))
  \uart_module/u_uart/rx_samples_1_dpot  (
    .I0(\uart_module/u_uart/_n0206_inv1_rstpot_470 ),
    .I1(\uart_module/u_uart/rx_samples [1]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/recv_state [0]),
    .I4(\uart_module/u_uart/recv_state [1]),
    .I5(\uart_module/u_uart/rx_samples [0]),
    .O(\uart_module/u_uart/rx_samples_1_dpot_472 )
  );
  LUT6 #(
    .INIT ( 64'h10F010F054F010F0 ))
  \uart_module/u_uart/_n0258_inv1  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(\uart_module/transmit_108 ),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/_n0258_inv )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/out3_1  (
    .I0(\uart_module/u_uart/rx_clk [15]),
    .I1(\uart_module/u_uart/rx_clk [13]),
    .I2(\uart_module/u_uart/rx_clk [14]),
    .I3(\uart_module/u_uart/rx_clk [0]),
    .I4(\uart_module/u_uart/out1_419 ),
    .I5(\uart_module/u_uart/out ),
    .O(\uart_module/u_uart/out36_502 )
  );
  LUT5 #(
    .INIT ( 32'h0000001F ))
  \uart_module/u_uart/_n0245_inv1_cepot_rstpot  (
    .I0(\uart_module/u_uart/out31_411 ),
    .I1(\uart_module/u_uart/out32_412 ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I3(\uart_module/u_uart/out34_414 ),
    .I4(\uart_module/u_uart/out3_410 ),
    .O(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 )
  );
  LUT6 #(
    .INIT ( 64'h0000000355555555 ))
  \uart_module/u_uart/_n0245_inv1_cepot_cepot  (
    .I0(N12),
    .I1(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<5> ),
    .I2(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<7> ),
    .I3(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<8> ),
    .I4(\uart_module/u_uart/rx_clk[16]_GND_3_o_sub_10_OUT<6> ),
    .I5(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .O(\uart_module/u_uart/_n0245_inv1_cepot_cepot_504 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_samples_0_dpot1  (
    .I0(\uart_module/u_uart/rx_samples [0]),
    .I1(\uart_module/u_uart/rx_samples_0_dpot_471 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_samples_0_dpot1_505 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_samples_1_dpot1  (
    .I0(\uart_module/u_uart/rx_samples [1]),
    .I1(\uart_module/u_uart/rx_samples_1_dpot_472 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_samples_1_dpot1_506 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_samples_2_dpot1  (
    .I0(\uart_module/u_uart/rx_samples [2]),
    .I1(\uart_module/u_uart/rx_samples_2_dpot_473 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_samples_2_dpot1_507 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_sample_countdown_1_dpot1  (
    .I0(\uart_module/u_uart/rx_sample_countdown [1]),
    .I1(\uart_module/u_uart/rx_sample_countdown_1_dpot_468 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_sample_countdown_1_dpot1_510 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_sample_countdown_2_dpot1  (
    .I0(\uart_module/u_uart/rx_sample_countdown [2]),
    .I1(\uart_module/u_uart/rx_sample_countdown_2_dpot_469 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_sample_countdown_2_dpot1_511 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_bits_remaining_2_dpot1  (
    .I0(\uart_module/u_uart/rx_bits_remaining [2]),
    .I1(\uart_module/u_uart/rx_bits_remaining_2_dpot_440 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_bits_remaining_2_dpot1_514 )
  );
  LUT3 #(
    .INIT ( 8'hCA ))
  \uart_module/u_uart/rx_bits_remaining_3_dpot1  (
    .I0(\uart_module/u_uart/rx_bits_remaining [3]),
    .I1(\uart_module/u_uart/rx_bits_remaining_3_dpot_441 ),
    .I2(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_bits_remaining_3_dpot1_515 )
  );
  LUT6 #(
    .INIT ( 64'hFFFF0000FFBF4000 ))
  \uart_module/u_uart/Mmux__n02591171  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I4(N14),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<3> )
  );
  LUT6 #(
    .INIT ( 64'hFFFF0000FFBF4000 ))
  \uart_module/u_uart/Mmux__n02591181  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I4(N17),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<4> )
  );
  LUT6 #(
    .INIT ( 64'hFFFF0000FFBF4000 ))
  \uart_module/u_uart/Mmux__n02591141  (
    .I0(\uart_module/u_uart/tx_state [1]),
    .I1(\uart_module/u_uart/tx_state [0]),
    .I2(uart_resetn_24),
    .I3(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I4(N20),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<12> )
  );
  LUT4 #(
    .INIT ( 16'hE2AA ))
  \uart_module/u_uart/rx_samples_3_dpot1  (
    .I0(\uart_module/u_uart/rx_samples [3]),
    .I1(\uart_module/u_uart/_n0206_inv1_rstpot_470 ),
    .I2(\uart_module/u_uart/_n0186 [3]),
    .I3(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_samples_3_dpot1_508 )
  );
  LUT6 #(
    .INIT ( 64'hDFFFAAAAAAAAAAAA ))
  \uart_module/u_uart/rx_sample_countdown_0_dpot1  (
    .I0(\uart_module/u_uart/rx_sample_countdown [0]),
    .I1(\uart_module/u_uart/recv_state [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/_n0226_inv1_rstpot_467 ),
    .I5(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_sample_countdown_0_dpot1_509 )
  );
  LUT4 #(
    .INIT ( 16'hE2AA ))
  \uart_module/u_uart/rx_sample_countdown_3_dpot1  (
    .I0(\uart_module/u_uart/rx_sample_countdown [3]),
    .I1(\uart_module/u_uart/_n0226_inv1_rstpot_467 ),
    .I2(\uart_module/u_uart/_n0209 [3]),
    .I3(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_sample_countdown_3_dpot1_512 )
  );
  LUT6 #(
    .INIT ( 64'h9000AAAAAAAAAAAA ))
  \uart_module/u_uart/rx_bits_remaining_1_dpot1  (
    .I0(\uart_module/u_uart/rx_bits_remaining [1]),
    .I1(\uart_module/u_uart/rx_bits_remaining [0]),
    .I2(\uart_module/u_uart/recv_state [1]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/_n0245_inv1_rstpot_439 ),
    .I5(\uart_module/u_uart/_n0245_inv1_cepot_rstpot_503 ),
    .O(\uart_module/u_uart/rx_bits_remaining_1_dpot1_513 )
  );
  LUT6 #(
    .INIT ( 64'hB2AAB2AABEAA8EAA ))
  \uart_module/u_uart/Mmux__n0259192  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(uart_resetn_24),
    .I4(\uart_module/u_uart/tx_bits_remaining[3]_reduce_or_66_o ),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_GND_3_o_wide_mux_78_OUT<0> )
  );
  LUT6 #(
    .INIT ( 64'hFFFF02AAFFFF32AA ))
  \uart_module/u_uart/Mmux__n02591121  (
    .I0(\uart_module/transmit_108 ),
    .I1(\uart_module/u_uart/tx_state [1]),
    .I2(\uart_module/u_uart/tx_state [0]),
    .I3(uart_resetn_24),
    .I4(N65),
    .I5(\uart_module/u_uart/n0066 ),
    .O(\uart_module/u_uart/tx_state[1]_tx_clk[12]_wide_mux_82_OUT<10> )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFFE ))
  \uart_module/u_uart/out3_2  (
    .I0(\uart_module/u_uart/rx_clk [15]),
    .I1(\uart_module/u_uart/rx_clk [13]),
    .I2(\uart_module/u_uart/rx_clk [14]),
    .I3(\uart_module/u_uart/rx_clk [0]),
    .I4(\uart_module/u_uart/out1_419 ),
    .I5(\uart_module/u_uart/out ),
    .O(\uart_module/u_uart/out37_516 )
  );
  LUT6 #(
    .INIT ( 64'hFFFFFFFFFFFFFFE0 ))
  \uart_module/u_uart/out37_1  (
    .I0(\uart_module/u_uart/out31_411 ),
    .I1(\uart_module/u_uart/out32_412 ),
    .I2(\uart_module/u_uart/rx_clk[16]_reduce_or_9_o ),
    .I3(\uart_module/u_uart/out35_415 ),
    .I4(\uart_module/u_uart/out34_414 ),
    .I5(\uart_module/u_uart/out3_410 ),
    .O(\uart_module/u_uart/out371 )
  );
  BUFGP   CLK_BUFGP (
    .I(CLK),
    .O(CLK_BUFGP_2)
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<12>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [12]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<12> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<11>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [11]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<11> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<10>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [10]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<10> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<9>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [9]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<9> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<8>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [8]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<8> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<7>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [7]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<7> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<6>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [6]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<6> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<5>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [5]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<5> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<4>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [4]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<4> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<3>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [3]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<3> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<2>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [2]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<2> )
  );
  INV   \uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<1>_INV_0  (
    .I(\uart_module/u_uart/tx_clk [1]),
    .O(\uart_module/u_uart/Msub_tx_clk[12]_GND_3_o_sub_13_OUT_lut<1> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<15>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [15]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<15> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<14>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [14]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<14> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<13>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [13]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<13> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<12>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [12]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<12> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<11>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [11]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<11> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<10>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [10]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<10> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<9>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [9]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<9> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<8>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [8]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<8> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<7>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [7]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<7> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<6>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [6]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<6> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<5>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [5]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<5> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<4>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [4]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<4> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<3>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [3]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<3> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<2>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [2]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<2> )
  );
  INV   \uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<1>_INV_0  (
    .I(\uart_module/u_uart/rx_clk [1]),
    .O(\uart_module/u_uart/Msub_rx_clk[16]_GND_3_o_sub_10_OUT_lut<1> )
  );
  INV   uart_resetn_inv1_INV_0 (
    .I(uart_resetn_24),
    .O(\uart_module/fifo_rst )
  );
  INV   read_valid_PWR_1_o_MUX_118_o1_INV_0 (
    .I(read_valid_33),
    .O(read_valid_PWR_1_o_MUX_118_o)
  );
  INV   \SWITCH<0>_inv1_INV_0  (
    .I(SWITCH_0_IBUF_0),
    .O(\SWITCH<0>_inv )
  );
  INV   reset_inv1_INV_0 (
    .I(reset_21),
    .O(reset_inv)
  );
  INV   \uart_module/PWR_2_o_need_to_send_MUX_101_o1_INV_0  (
    .I(\uart_module/need_to_send_130 ),
    .O(\uart_module/PWR_2_o_need_to_send_MUX_101_o )
  );
  fifo   \uart_module/reading_fifo  (
    .rst(\uart_module/fifo_rst ),
    .wr_clk(CLK_BUFGP_2),
    .rd_clk(CLK_BUFGP_2),
    .wr_en(\uart_module/r_wr_en_132 ),
    .rd_en(\uart_module/r_rd_en_105 ),
    .full(\uart_module/r_full ),
    .empty(\uart_module/r_empty ),
    .valid(\uart_module/r_fifo_read_valid ),
    .din({LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, LED_3_OBUF_28, 
\uart_module/rf_data_in [7], \uart_module/rf_data_in [6], \uart_module/rf_data_in [5], \uart_module/rf_data_in [4], \uart_module/rf_data_in [3], 
\uart_module/rf_data_in [2], \uart_module/rf_data_in [1], \uart_module/rf_data_in [0]}),
    .dout({\uart_module/rf_data_out [15], \uart_module/rf_data_out [14], \uart_module/rf_data_out [13], \uart_module/rf_data_out [12], 
\uart_module/rf_data_out [11], \uart_module/rf_data_out [10], \uart_module/rf_data_out [9], \uart_module/rf_data_out [8], \uart_module/rf_data_out [7]
, \uart_module/rf_data_out [6], \uart_module/rf_data_out [5], \uart_module/rf_data_out [4], \uart_module/rf_data_out [3], \uart_module/rf_data_out [2]
, \uart_module/rf_data_out [1], \uart_module/rf_data_out [0]})
  );
  fifo   \uart_module/writing_fifo  (
    .rst(\uart_module/fifo_rst ),
    .wr_clk(CLK_BUFGP_2),
    .rd_clk(CLK_BUFGP_2),
    .wr_en(\uart_module/wr_en_106 ),
    .rd_en(\uart_module/rd_en ),
    .full(\uart_module/full ),
    .empty(\uart_module/empty ),
    .valid(\uart_module/fifo_read_valid ),
    .din({\uart_module/wf_data_in [15], \uart_module/wf_data_in [14], \uart_module/wf_data_in [13], \uart_module/wf_data_in [12], 
\uart_module/wf_data_in [11], \uart_module/wf_data_in [10], \uart_module/wf_data_in [9], \uart_module/wf_data_in [8], \uart_module/wf_data_in [7], 
\uart_module/wf_data_in [6], \uart_module/wf_data_in [5], \uart_module/wf_data_in [4], \uart_module/wf_data_in [3], \uart_module/wf_data_in [2], 
\uart_module/wf_data_in [1], \uart_module/wf_data_in [0]}),
    .dout({\NLW_uart_module/writing_fifo_dout<15>_UNCONNECTED , \NLW_uart_module/writing_fifo_dout<14>_UNCONNECTED , 
\NLW_uart_module/writing_fifo_dout<13>_UNCONNECTED , \NLW_uart_module/writing_fifo_dout<12>_UNCONNECTED , 
\NLW_uart_module/writing_fifo_dout<11>_UNCONNECTED , \NLW_uart_module/writing_fifo_dout<10>_UNCONNECTED , 
\NLW_uart_module/writing_fifo_dout<9>_UNCONNECTED , \NLW_uart_module/writing_fifo_dout<8>_UNCONNECTED , \uart_module/wf_data_out [7], 
\uart_module/wf_data_out [6], \uart_module/wf_data_out [5], \uart_module/wf_data_out [4], \uart_module/wf_data_out [3], \uart_module/wf_data_out [2], 
\uart_module/wf_data_out [1], \uart_module/wf_data_out [0]})
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
