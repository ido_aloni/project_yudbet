`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:34:43 02/03/2018 
// Design Name: 
// Module Name:    Encryptor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Encryptor(

		input wire clk,
		input wire resetn,
		input wire init,

		input wire [DATA_LEN:0] data,
		input wire data_valid,
		
		input wire [KEY_SIZE:0] key,
		input wire init_key,
		input wire new_key,
		
		input wire operation, // 1 encryption, 0 decryption
		input wire [7:0] encryption_type, 
		input wire [3:0] parameters,
		
		output reg [DATA_LEN:0] data_out,
		output reg data_out_valid
    );

/*
	- parameters: 
	0 (lsb) is the encrypt flag
	1 is the retransmit flag
	2 is the concatenation flag
	4 is the key-switch flag
	
	- encryption_type:
	0 (lsb) is the xor encryption
	1: not encrypting
	2: "special encryption" 
*/

parameter DATA_LEN = 128;
parameter KEY_SIZE = DATA_LEN;

//// - encryption params - ////

localparam ENCRYPTION_TYPE_REGXORS = 8'd0;
localparam ENCRYPTION_TYPE_SPECIAL = 8'd2;

//// - logical vars - ////

reg in_process;
reg done_operation;
reg used_key;

// the previous key
reg [KEY_SIZE:0] prev_key;
reg [KEY_SIZE:0] curr_key;

// raw data for decryption //
reg [DATA_LEN:0] raw_data;



///////// AES /////////////


reg encdec; // 1 = enc, 0 = dec
reg aes_init; // init
reg aes_next; // command to do action
wire aes_ready; // ready to perform action
wire [127:0] aes_key = curr_key;
wire aes_keylen = 0; // 1 256 bit keylen, 0 128 keylen
wire [127:0] aes_block = data;
wire [127:0] aes_result;
wire aes_res_valid;
/*
aes_core a(
	.clk(clk),
	.reset_n(resetn),
	
	.encdec(encdec),
	.init(aes_init),
	.next(aes_next),
	.ready(aes_ready),
	
	.key(aes_key),
	.keylen(aes_keylen),
	
	.block(aes_block),
	.result(aes_result),
	.result_valid(aes_res_valid)
);*/


reg [0:4] aes_state; 
reg [0:4] aes_next_state;

localparam AES_STATE_INIT = 0;
localparam AES_STATE_WAITING_VALID = 1;
localparam AES_STATE_IN_PROCESS = 2;
localparam AES_STATE_DONE = 3;


//// - logical always blocks - ////

always @(posedge clk, negedge resetn, posedge init) begin

	if (!resetn) begin
	
		in_process <= 1'b0;
		
	end
	else if (init) begin
		
		in_process <= 1'b0;
		

	end
	else if(!init) begin
		
		if (data_valid) begin
			in_process <= 1'b1;
		end
		if (data_out_valid) begin
			in_process <= 1'b0;
		end
		
		
	end
end

always @(posedge clk, negedge resetn, posedge init) begin
	
	if (!resetn || init) begin
		data_out <= 0;
		raw_data <= 0;
		done_operation <= 1'b0;
		used_key <= 1'b0;
		data_out_valid <= 1'b0;
		
		aes_state <= 0;
		
	end
	else if (!in_process) begin
		data_out_valid <= 1'b0;
		done_operation <= 1'b0;
		if (init_key) begin
			curr_key <= key;
		end
	end
	else begin
		if (parameters[0] && !parameters[1] && !done_operation) begin // parameters[0] is the encrypt flag, as mentioned before
			
			$display("Here inside encrypting stuff");
			
			case (encryption_type) 
				
				ENCRYPTION_TYPE_REGXORS : begin
					
					done_operation <= 1'b1;
					
					if (operation) begin
						if (!parameters[2]) 
							data_out <= data ^ curr_key;
						else begin
							data_out <= data ^ curr_key ^ data_out;
						end
						prev_key <= curr_key;
						curr_key <= curr_key + 1;
						data_out_valid <= 1'b1;
						used_key <= 1'b1;
						data_out_valid <= 1'b1;
					end
					else begin
						if (!parameters[2]) 
							data_out <= data ^ curr_key;
						else begin
							data_out <= data ^ curr_key ^ raw_data;
						end
						raw_data <= data;
						prev_key <= curr_key;
						curr_key <= curr_key + 1;
						data_out_valid <= 1'b1;
						used_key <= 1'b1;
						data_out_valid <= 1'b1;
					end
				end
				
				ENCRYPTION_TYPE_SPECIAL : begin
					
					$display("In special enc case %d", aes_state);
					
					
					case (aes_state) 
						
						AES_STATE_INIT : begin
							
							raw_data <= data;
							data_out_valid <= 1'b0;
							$display("state init");
							
							if (operation) 
								encdec <= 1;
							else
								encdec <= 0;
							aes_init <= 1;
							
							aes_state <= AES_STATE_WAITING_VALID;
							aes_next_state <= AES_STATE_IN_PROCESS;
							
						end
					
						AES_STATE_WAITING_VALID : begin
							
							aes_init <= 0;
							$display("state waiting valid");
							
							if (aes_ready)
								aes_state <= aes_next_state;
						
						end
						
						AES_STATE_IN_PROCESS : begin
						
							$display("state in process");
							aes_next_state <= AES_STATE_DONE;
							aes_next <= 1;
							if (aes_next == 1) begin
								aes_next <= 0;
							end
							
							if (aes_res_valid) begin
								aes_state <= AES_STATE_WAITING_VALID;
							end
						
						end
						
						AES_STATE_DONE : begin
							
							$display("state done");
							done_operation <= 1'b1;
							aes_state <= AES_STATE_INIT;
							data_out_valid <= 1'b1;
							
							prev_key <= curr_key;
							curr_key <= curr_key + 1;
							
							used_key <= 1'b1;
							
							if (parameters[2]) begin
								if (operation)
									data_out <= aes_result ^ data_out;
								else 
									data_out <= aes_result ^ raw_data;
							end
							else begin
								data_out <= aes_result;
							end
							
						end
					
					endcase
					
				end
				
			endcase
			
		end
		else if (parameters[1] && !done_operation) begin // parameters[1] is the retransmit flag
			
			case (encryption_type) 
				
				ENCRYPTION_TYPE_REGXORS : begin
					
					done_operation <= 1'b1;
					
					data_out <= data;
					
					if (used_key && parameters[3]) begin
						
						used_key <= 1'b0;
						curr_key <= prev_key;
					
					end
					
					data_out_valid <= 1'b1;
					
				end
				
				ENCRYPTION_TYPE_SPECIAL : begin
					
					done_operation <= 1'b1;
					
					data_out <= data;
					
					if (used_key && parameters[3]) begin
						
						used_key <= 1'b0;
						curr_key <= prev_key;
					
					end
					
					data_out_valid <= 1'b1;
					
				end
				
			endcase
			
		end
	end

end

endmodule
