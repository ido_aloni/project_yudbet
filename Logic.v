`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:49:22 01/13/2018 
// Design Name: 
// Module Name:    Logic 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Logic(
		
		input clk,
		input resetn,
		
		input [7:0] byte_in,
		output reg byte_in_valid,
		input wire can_read,
		input wire [CRC_WIDTH-1:0] crc_register,
		
		input wire can_write,
		output reg [7:0] byte_out,
		output reg byte_out_valid,
		
		output reg [7:0] monitor
		
    );

///////////// FUNCTIONS ///////////////

 function integer log2(input integer M);
	  integer i;
 begin
	  log2 = 1;
	  for (i = 0; 2**i <= M; i = i + 1)
			log2 = i + 1;
 end endfunction

///////////// PARAMS ///////////////

parameter CRC_WIDTH = 32;
parameter DATA_WIDTH = 8;
parameter ADDR_WIDTH = log2(DATA_DEPTH);
parameter DATA_DEPTH = PACKET_LENGTH;

reg [32:0] status;
reg [DATA_WIDTH*DATA_DEPTH-1:0] packet;
reg [DATA_WIDTH*SESSION_BEGIN_RESP_SIZE-1:0] session_begin_resp_packet;
reg [7:0] packet_type;
reg [FRAGMENT_SIZE:0] key;


///////// VALIDATOR ///////////

wire [CRC_WIDTH-1:0] mask = 32'h04C11DB7;
reg [7:0] validator_data;
reg [7:0] validator_gap_counter;
reg validator_data_valid;
reg validator_init;
reg validation_check_done;
reg built_packet;
reg push_last_byte_to_validator;

reg last_received_packet_valid;
reg validator_data_ready;

reg [31:0] validator_counter;
localparam validtor_final_push_value = CRC_WIDTH / 8; // example: When the crc width is 16 bit, we'll need to push in at least 2 more zeroed out bytes. Pushing more than that would not change the crc's result.
localparam VALIDATOR_GAP = 8'd12;

wire [CRC_WIDTH-1:0] crc_out;

validator #(.LEN(CRC_WIDTH)) v(
	.clk(clk),
	.resetn(resetn),
	.data(validator_data),
	.init(validator_init),
	.mask(mask),
	.data_valid(validator_data_valid),
	.crc_out(crc_out)
);

///////// LOGIC PARAMETERS ///////////

//// - status params - ////
localparam STATUS_ON_RESET = 32'd0;

localparam STATUS_BEGIN_SESSION_RECEIVING_HEADER = 32'd1;
localparam STATUS_BEGIN_SESSION_ANALYZING_HEADER_BUILDING_RESP = 32'd2;
localparam STATUS_BEGIN_SESSION_SENDING_RESP = 32'd3;
localparam STATUS_FINISHED_SESSION_SENDING_RESP = 32'd4;

localparam STATUS_RECEIVING_ADDITIONAL_PARAMS = 32'd5;

localparam STATUS_RECEIVING_PACKET = 32'd6;
localparam STATUS_HANDLING_PACKET = 32'd7;
localparam STATUS_BUILDING_PACKET_HEADER = 32'd10;
localparam STATUS_ENCRYPTING = 32'd11;
localparam STATUS_FINISHED_BUILDING_PACKET = 32'd12;
localparam STATUS_SENDING_BACK_PACKET = 32'd8;

localparam STATUS_PENDING_TO_PACKET = 32'd9;

//// - lengths - ////
localparam SESSION_BEGIN_HEADER_SIZE = 21; // in bytes.
localparam SESSION_BEGIN_RESP_SIZE = 23;

localparam FRAGMENT_SIZE = 64'd128; // 128sss
localparam PACKET_HEADER_SIZE = 64'd14; // in bytes.
localparam PACKET_LENGTH = FRAGMENT_SIZE + PACKET_HEADER_SIZE;

//// - headers & responses values - ////
localparam RESP_TYPE_AVALIABLE = 8'd0;
localparam RESP_RECV_VALID = 8'd0;
localparam RESP_RECV_INVALID = 8'd1;
localparam WIN_LENGTH = 8'd1; // Current win length

localparam APPENDED_VALUES = 32'd0;
localparam RESERVED = 32'd0;

//// - packet types - ////

localparam TYPE_RECV_VALID = 8'd0;
localparam TYPE_RECV_INVALID = 8'd1;
localparam TYPE_RETRANSMIT = 8'd2;
localparam TYPE_KEY = 8'd3;

//// - flags - ////

localparam LAST_PACKET_FLAG = 8'b00000100;
localparam ENCRYPT_FLAG = 8'b00000010;



///////// ENCRYPTOR ///////////	

/*
		input wire clk,
		input wire resetn,

		input wire [DATA_LEN:0] data,
		input wire data_valid,
		
		input wire [KEY_SIZE:0] key,
		input wire init_key,
		
		input wire operation, // 1 encryption, 0 decryption
		input wire [7:0] encryption_type, 
		input wire [3:0] parameters,
		
		output reg [DATA_LEN:0] data_out,
		output wire data_out_valid
*/

reg encryptor_data_valid;
reg encryptor_init_key;
reg encryptor_operation;
reg [7:0] encryptor_type;
reg [3:0] encryptor_parameters;
reg encryptor_init;
reg need_to_cat;
reg check_derive; // Some logical lever used to derive the beginning of a check.

reg need_to_switch_keys;

wire [FRAGMENT_SIZE*8:0] encryptor_data_out;
wire encryptor_data_out_valid;

reg writing_encryption_output;

Encryptor 
#(
.DATA_LEN(FRAGMENT_SIZE*8)
)
encryptor(

	.clk(clk),
	.resetn(resetn),
	.init(encryptor_init),
	
	.data(packet[112+:FRAGMENT_SIZE*8]),
	.data_valid(encryptor_data_valid),
	
	.key(key),
	.init_key(encryptor_init_key),
	
	.operation(encryptor_operation),
	.encryption_type(encryptor_type),
	.parameters(encryptor_parameters),
	
	.data_out(encryptor_data_out),
	.data_out_valid(encryptor_data_out_valid)

);


////////// LOGIC ALWAYS ///////////

reg need_to_print;

//// - logic vars - //// 
reg [8*ADDR_WIDTH + 1:0] packet_writing_pointer;
reg [8*ADDR_WIDTH + 1:0] packet_reading_pointer;
reg current_packet_valid;
reg received_packet;
reg read_one_byte_flag;
reg need_to_increment;

localparam PENDING_TIME = 100000 * 800; // 100000 = 1ms / 10ns, we're wating 800ms here because each clock cycle has a 10ns cycle time
reg [31:0] pending_counter;

reg [5*DATA_WIDTH:0] resp_packet_writing_pointer;
reg [5*DATA_WIDTH:0] resp_packet_reading_pointer;

reg need_to_write;
reg need_to_read;

reg end_of_proccess;

//// - always - ////


//// packet writing /////

localparam MAX_WRITING_SIZE = 32;

reg packet_we;
reg [7:0] pw_width;
reg [MAX_WRITING_SIZE-1:0] packet_data_in;
reg [0:15] writing_counter;

always @(packet) begin
	
		$display("Writing %h to location %d", packet_data_in[0+:8], packet_writing_pointer);
end
/*
always @(posedge clk) begin
	if (packet_we) begin
		if (pw_width == 7'd8) begin
			packet[packet_writing_pointer+:8] <= packet_data_in[0+:8];
		end
		else if (pw_width == 7'd32) begin
			packet[packet_writing_pointer+:32] <= packet_data_in[0+:32];
		end
	end
end*/


//// LOGIC always ////

always @(posedge clk, negedge resetn) begin

	if (!resetn) begin
		
		status <= STATUS_ON_RESET;
		packet_type <= 8'd0;
		key <= 0;
		packet_we <= 0;
		
		monitor<=0;
		
		writing_counter<=0;
		packet_data_in <= 0;
		pw_width <= 8'd0;
		packet_we <= 1'b0;
		packet_writing_pointer <= 0;
		packet_reading_pointer <= 0;
		resp_packet_writing_pointer <= 5'd0;
		resp_packet_reading_pointer <= 5'd0;
		current_packet_valid <= 1'b0;
		received_packet <= 1'b0;
		read_one_byte_flag <= 1'b0;
		need_to_increment <= 1'b0;
		push_last_byte_to_validator <= 1'b0;
		validator_init <= 1'b0;
		
		validator_data_valid <= 1'b0;
		validator_data <= 8'd0;
		validator_counter <= 0;
		validation_check_done <= 1'b0;
		built_packet <= 1'b0;
		validator_gap_counter <= 8'd0;
		validator_data_ready <= 1'b0;
		
		pending_counter <= 32'd0;
		last_received_packet_valid <= 1'b0;
		
		byte_out_valid <= 1'b0;
		byte_in_valid <= 1'b0;
		
		need_to_write <= 1'b1;
		need_to_read <= 1'b1;
		
		writing_encryption_output <= 1'b0;
		encryptor_data_valid <= 0;
		encryptor_init_key <= 0;
		encryptor_operation <= 0;
		encryptor_type <= 0;
      encryptor_parameters <= 0;
		encryptor_init <= 1'b1;
		need_to_cat <= 1'b0;
		need_to_switch_keys <= 1'b0;
		
	end
	else begin
		
		encryptor_init <= 1'b0;
		
		case (status) 
			
			STATUS_ON_RESET : begin
				
				$display("On reset");
				
				// This is the logical reset.
				status  <= STATUS_BEGIN_SESSION_RECEIVING_HEADER;
				packet_type <= 8'd0;
				key <= 0;
				
				writing_counter<=0;
				packet_data_in <= 0;
				pw_width <= 8'd0;
				packet_we <= 1'b0;
				packet_writing_pointer <= 0;
				packet_reading_pointer <= 0;
				resp_packet_writing_pointer <= 5'd0;
				resp_packet_reading_pointer <= 5'd0;
				current_packet_valid <= 1'b0;
				built_packet <= 1'b0;
				received_packet <= 1'b0;
				read_one_byte_flag <= 1'b0;
				need_to_increment <= 1'b0;
				push_last_byte_to_validator <= 1'b0;
				validator_init <= 1'b0;
				
				validator_data_valid <= 1'b0;
				validator_data  <= 8'd0;
				validator_counter <= 8'd0;
				validation_check_done <= 1'b0;
				validator_gap_counter <= 8'd0;
				validator_data_ready <= 1'b0;
				
				pending_counter <= 32'd0;
				last_received_packet_valid <= 1'b0;
				
				byte_out_valid <= 1'b0;
				byte_in_valid <= 1'b0;
				
				need_to_write <= 1'b1;
				need_to_read <= 1'b1;
				
				need_to_print <= 1'b0;
				
				writing_encryption_output <= 1'b0;
				encryptor_data_valid <= 0;
				encryptor_init_key <= 0;
				encryptor_operation <= 0;
				encryptor_type <= 0;
				encryptor_parameters <= 0;
				need_to_cat <= 1'b0;
				need_to_switch_keys <= 1'b0;
				
			end
			
			STATUS_BEGIN_SESSION_RECEIVING_HEADER : begin
				
				packet_we <= 1'b0;
				
				if (!received_packet ) begin
					
					validator_data_valid <= 1'b0;
					
					if (can_read && need_to_read) begin
						
						
						byte_in_valid <= 1'b1;
						
						need_to_increment <= 1'b0;
						
						if ( !read_one_byte_flag )
							read_one_byte_flag <= 1'b1;
						else need_to_increment <= 1'b1;
						
						need_to_read <= 1'b0;
						
						$display("%d writing to the %d packet", ADDR_WIDTH, packet_writing_pointer>>3);
						need_to_print <= 1'b1;
						
					end
					else begin
						
						if (need_to_increment && (packet_writing_pointer < 136 || packet_writing_pointer  > 136 + CRC_WIDTH)) begin
							//$display("Pushing %h to validator", byte_in);
							validator_data <= byte_in;
							validator_data_valid <= 1'b1;
						end
						
						
						packet[packet_writing_pointer+:8] <= byte_in;
						/*packet_data_in[0+:8] <= byte_in;
						packet_we <= 1'b1;
						pw_width <= 8'd8;*/
						
						
						if (need_to_print ) 
							//$display("Received: %h", byte_in);
						need_to_print <= 1'b0;
						
						if ( need_to_increment) 
							packet_writing_pointer <= packet_writing_pointer + 8;
						
						need_to_increment <= 1'b0;
						
						if (packet_writing_pointer >> 3 >= SESSION_BEGIN_HEADER_SIZE - 1) begin 
							//$display("last Received: %h", byte_in);
							received_packet <= 1'b1;
						end
							
						
						need_to_read <= 1'b1;
						byte_in_valid <= 1'b0;
					end
				end
				else begin
					
					byte_in_valid <= 1'b0;
					if (validator_counter == validtor_final_push_value+1) begin
						
						// reset everything
						
						status <= STATUS_BEGIN_SESSION_ANALYZING_HEADER_BUILDING_RESP;
						received_packet <= 1'b0;
						validator_counter <= 8'd0;
						
						
						need_to_increment <= 1'b0;
						read_one_byte_flag <= 1'b0;
						packet_writing_pointer <= 0;
						need_to_read <= 1'b1;
						validator_counter <= 0;
						validator_gap_counter <= 0;
						
						
					end
					else begin
						validator_data_valid <= 1'b0;
						
						if (validator_gap_counter == VALIDATOR_GAP) begin 
							validator_data_valid <= 1'b1;
							// validator_data <= packet[validator_counter*8 + 136+:8];
							validator_data <= 8'd0;
							validator_counter <= validator_counter + 8'd1;
							validator_gap_counter <= 8'd0;
						end
						else validator_gap_counter <= validator_gap_counter + 8'd1;
					end
					read_one_byte_flag <= 1'b0;
					packet_writing_pointer <= 0;
					need_to_read <= 1'b1;
				end
				
			end
			
			STATUS_BEGIN_SESSION_ANALYZING_HEADER_BUILDING_RESP : begin
				
				if (!built_packet) begin
					validator_init <= 1'b1;
					built_packet <= 1'b1;
					packet_type <= packet[0];
					session_begin_resp_packet[7:0] <= RESP_TYPE_AVALIABLE;
					
					
					$display("header packet method is: %d", packet[8+:8]);
					
					if (packet[9])
						encryptor_type <= 0;
					
					if (packet[8])
						encryptor_type <= 1; 
						
					if (packet[11])
						encryptor_type <= 2;
					
					if (packet[10])
						encryptor_operation <= 1;
						
					
					need_to_cat <= packet[15];
					
					
					// Checking the CRC and updating the response packet.
					//$display("CRC is: %h, packet crc is: %h", crc_out, packet[136+:(CRC_WIDTH)]); 
					if (crc_out == packet[136+:(CRC_WIDTH)]) begin
						session_begin_resp_packet[15:8] <= RESP_RECV_VALID;
						last_received_packet_valid <= 1'b1;
					end
					else begin
						session_begin_resp_packet[15:8] <= RESP_RECV_INVALID;
						last_received_packet_valid <= 1'b0;
					end
					session_begin_resp_packet[23:16] <= WIN_LENGTH;
					session_begin_resp_packet[87 : 24] <= FRAGMENT_SIZE;
					session_begin_resp_packet[119 : 88] <= APPENDED_VALUES;
					session_begin_resp_packet[151 : 120] <= 32'd0;
					session_begin_resp_packet[183 : 121] <= RESERVED;
				end
				else if (validation_check_done) begin
					
					validation_check_done <= 1'b0;
					status <= STATUS_BEGIN_SESSION_SENDING_RESP;		
					validator_init <= 1'b1;
					validator_counter <= 1'b0;
					session_begin_resp_packet[151 : 120] <= crc_out;
					$display("Crc of resp is: %h", crc_out);
				end
				else begin
					validation_check_done <= 1'b0;
					validator_data_valid <= 1'b0;
					validator_init <= 1'b0;
					if (validator_gap_counter >= VALIDATOR_GAP) begin
						validator_gap_counter <= 0;
						if ((validator_counter < SESSION_BEGIN_RESP_SIZE)) begin
							if (validator_counter < 15 || validator_counter > 15 + CRC_WIDTH/8) begin
								validator_data_valid <= 1'b1;
								validator_data <= session_begin_resp_packet[validator_counter*8+:8];
							end
						end
						else if ((validator_counter-SESSION_BEGIN_RESP_SIZE) < validtor_final_push_value) begin
							validator_data <= 8'd0;
							validator_data_valid <= 1'b1;
						end
						else begin
							validation_check_done <= 1'b1;
						end
						validator_counter <= validator_counter + 8'd1;
					end
					else begin
						validator_gap_counter <= validator_gap_counter + 1;
					end
				end
				
			end
			
			STATUS_BEGIN_SESSION_SENDING_RESP : begin
				
				if (0) begin // Check if the packet's data is valid, according to the crc
					
				end
				else begin
					// Send the response packet.
					
					if (resp_packet_reading_pointer>>3 < SESSION_BEGIN_RESP_SIZE) begin
						if (can_write && need_to_write) begin
							byte_out[0+:8] <= session_begin_resp_packet[resp_packet_reading_pointer+:8];
							byte_out_valid <= 1'b1;
							need_to_write <= 1'b0;
							resp_packet_reading_pointer <= resp_packet_reading_pointer + 5'd8;
						end
						else begin
							need_to_write <= 1'b1;
							byte_out_valid <= 1'b0;
						end
					end
					else begin
						status <= STATUS_FINISHED_SESSION_SENDING_RESP;
						validator_init <= 1'b1;
						resp_packet_reading_pointer <= 5'd0;
						byte_out_valid <= 1'b0;
						need_to_write<= 1'b1;
					end
					
				end
				
			end
			
			STATUS_FINISHED_SESSION_SENDING_RESP : begin
				
				$display("finished sending resp");
				
				if (last_received_packet_valid)
					status <= STATUS_PENDING_TO_PACKET;
				else 
					status <= STATUS_ON_RESET;
					
				last_received_packet_valid <= 1'b0;
				packet_writing_pointer <= 0;
				
			end
		
			STATUS_PENDING_TO_PACKET : begin
			
				if (can_read && pending_counter < PENDING_TIME) begin
					status <= STATUS_RECEIVING_ADDITIONAL_PARAMS;
					$display("Finished pending");
					pending_counter <= 0;
				end
				else if (pending_counter >= PENDING_TIME) begin
					status <= STATUS_ON_RESET;
				end
				else begin
					pending_counter <= pending_counter + 1;
				end
				
			end
		
			STATUS_RECEIVING_ADDITIONAL_PARAMS : begin
			
				status <= STATUS_RECEIVING_PACKET;
			
			end
			
			STATUS_RECEIVING_PACKET : begin
				
				packet_we <= 1'b0;
				
				if (!received_packet ) begin
					
					validator_data_valid <= 1'b0;
					validator_init <= 1'b0;
					
					if (!push_last_byte_to_validator && can_read && need_to_read) begin
						
						
						byte_in_valid <= 1'b1;
						
						need_to_increment <= 1'b0;
						
						if ( !read_one_byte_flag )
							read_one_byte_flag <= 1'b1;
						else need_to_increment <= 1'b1;
						
						need_to_read <= 1'b0;
						
						need_to_print <= 1'b1;
						
					end
					else if (!push_last_byte_to_validator) begin
						
						if (need_to_increment && (packet_writing_pointer < 64 || packet_writing_pointer  >= 64 + CRC_WIDTH)) begin
							
							validator_data <= byte_in;
							validator_data_valid <= 1'b1;
						end
						
						packet[packet_writing_pointer+:8] <= byte_in;
						/*packet_data_in[0+:8] <= byte_in;
						packet_we <= 1'b1;
						pw_width <= 8'd8;*/
						
						if (need_to_print) 
							$display("Received: %h %d", byte_in, packet_writing_pointer);
						need_to_print <= 1'b0;
						
						if ( need_to_increment) 
							packet_writing_pointer <= packet_writing_pointer + 8;
						
						need_to_increment <= 1'b0;
						
						if (packet_writing_pointer >> 3 >= PACKET_LENGTH - 1) begin 
							push_last_byte_to_validator <= 1'b1;
							//received_packet <= 1'b1;
						end
							
						
						need_to_read <= 1'b1;
						byte_in_valid <= 1'b0;
					end
					else begin
						
						
						
						if (validator_gap_counter >= VALIDATOR_GAP) begin
							validator_data <= byte_in;
							
							if (validator_gap_counter == VALIDATOR_GAP)
								validator_data_valid <= 1'b1;
							else
								validator_data_valid <= 1'b0;
							
							if (validator_gap_counter == 2*VALIDATOR_GAP) begin 
								received_packet <= 1'b1;
								validator_data_valid <= 1'b0;
								push_last_byte_to_validator <= 1'b1;
								validator_gap_counter <= 8'd0;
							end
							else validator_gap_counter <= validator_gap_counter + 8'd1;
							end
						else validator_gap_counter <= validator_gap_counter + 8'd1;
					end
				end
				else begin
					
					byte_in_valid <= 1'b0;
					if (validator_counter == validtor_final_push_value+1) begin
						// reset everything
						status <= STATUS_HANDLING_PACKET;
						
						packet[64+:CRC_WIDTH] <= crc_out;
						/*packet_writing_pointer <= 64;
						packet_data_in[0+:CRC_WIDTH] <= crc_out;
						packet_we <= 1'b1;
						pw_width <= 8'd32;*/
						
						validator_data_valid <= 1'b0;
						
						if (crc_out[CRC_WIDTH-1-:CRC_WIDTH] == packet[64+:CRC_WIDTH]) begin
							last_received_packet_valid <= 1'b1;
						end
						else begin
							last_received_packet_valid <= 1'b0;
						end
						
						validator_init <= 1'b1;
						received_packet <= 1'b0;
						validator_counter <= 8'd0;
						push_last_byte_to_validator <= 1'b0;
						
						need_to_increment <= 1'b0;
						read_one_byte_flag <= 1'b0;
						
						need_to_read <= 1'b1;
						validator_counter <= 0;
						validator_gap_counter <= 0;
					end
					else begin
						validator_data_valid <= 1'b0;
						
						if (validator_gap_counter == VALIDATOR_GAP) begin 
							validator_data_valid <= 1'b1;
							// validator_data <= packet[validator_counter*8 + 136+:8];
							validator_data <= 8'd0;
							validator_counter <= validator_counter + 8'd1;
							validator_gap_counter <= 8'd0;
						end
						else validator_gap_counter <= validator_gap_counter + 8'd1;
					end
					read_one_byte_flag <= 1'b0;
					//packet_writing_pointer <= 0;
					need_to_read <= 1'b1;
				end
			
			end
			
			STATUS_HANDLING_PACKET : begin
			
				packet_writing_pointer <= 0;
				packet_we <= 1'b0;
				status <= STATUS_BUILDING_PACKET_HEADER;
				$display("recieved packet crc out is: %h", packet[64+:CRC_WIDTH]);
				validator_init <= 1'b0;
				
			end
			
			STATUS_BUILDING_PACKET_HEADER : begin
				
				packet_we <= 1'b0;
				
				
				$display("last recieved packet valid flag is: %d", last_received_packet_valid);
				monitor<=monitor+1;
				
				if (last_received_packet_valid) begin
					
					packet[96+:8] <= TYPE_RECV_VALID;
					/*packet_data_in[0+:8] <= TYPE_RECV_VALID;
					packet_writing_pointer <= 96;
					packet_we <= 1'b1;
					pw_width <= 8'd8;*/
					
					need_to_switch_keys <= 1'b1;
					
					if (packet[0+:8] == 8'd0 ) 
						status <= STATUS_FINISHED_BUILDING_PACKET;
					else
						status <= STATUS_ENCRYPTING;
					
					
					if (packet[96+:8] == TYPE_KEY || packet[0+:8] == 8'd0) begin
						key <= packet[112+:FRAGMENT_SIZE];
						encryptor_init_key <= 1'b1;
					end
					else begin
						encryptor_init_key <= 1'b0;
					end
					
					encryptor_parameters[0] <= packet[104+1]; // encrypt flag
					// retransmit flag
					if (packet[96+:8] == TYPE_RETRANSMIT ) begin
						encryptor_parameters[1] <= 1'b1;
						if (need_to_switch_keys)
							encryptor_parameters[3] <= 1'b1;
					end
					else begin 
						encryptor_parameters[1] <= 1'b0;
						encryptor_parameters[3] <= 1'b0;
					end
					
					if (packet[0+:8] == 8'd1 || !need_to_cat) 
						encryptor_parameters[2] <= 1'b0;
					else 
						encryptor_parameters[2] <= 1'b1;
						
					
				end	
				else begin
					packet[96+:8] <= TYPE_RECV_INVALID;/*
					packet_data_in[0+:8] <= TYPE_RECV_INVALID;
					packet_writing_pointer <= 96;
					packet_we <= 1'b1;
					pw_width <= 8'd8;*/
					status <= STATUS_FINISHED_BUILDING_PACKET;
					need_to_switch_keys <= 1'b0;
					writing_counter <= 0;
				end
				
			end
			
			STATUS_ENCRYPTING : begin
					
					//$display("In encrypting");
					//$display("Encryptor params are: %b", encryptor_parameters);

					encryptor_init <= 1'b0;
					encryptor_data_valid <= 1'b1;
					packet_we <= 1'b0;
					
					if (encryptor_data_out_valid) begin 

						encryptor_data_valid <= 1'b0;
						writing_counter <=0;
						packet_writing_pointer<=112;
						writing_encryption_output <= 1'b1;
						
						$display("Here in this enc thing %d", writing_counter);
						// packet[112+:FRAGMENT_SIZE] <= encryptor_data_out;
						
					end
					else if (writing_encryption_output) begin
						
						encryptor_data_valid <= 1'b0;
						
						if (writing_counter >> 3 <FRAGMENT_SIZE) begin
							packet[112+:FRAGMENT_SIZE] <= encryptor_data_out;
							/*packet_data_in[0+:8] <= encryptor_data_out[writing_counter+:8];
							packet_writing_pointer <= writing_counter + 112;*/
							writing_counter <= FRAGMENT_SIZE<<3;
							$display("writing_counter >> 3 <FRAGMENT_SIZE,    %d", writing_counter);
							/*packet_we <= 1'b1;
							pw_width <= 8'd8;*/
						end
						else begin
							status <= STATUS_FINISHED_BUILDING_PACKET;
							writing_encryption_output <= 1'b0;
							writing_counter <=0;
							packet_writing_pointer<=0;
							$display("\nfinished encrypting\n");
							$display("\n\n\n finished encrypting: res is %h\n\n\n", encryptor_data_out);
						end
					end
			end
			
			STATUS_FINISHED_BUILDING_PACKET : begin
			
				// TODO: Here i need to calc the crc of the packet coming back
				
				
				if (validator_init) begin
					validator_init <= 1'b0;
					validator_gap_counter <= 0;
					
				end
				validator_data_valid <= 1'b0;
				
				if (validator_data_ready) begin
					validator_data_valid <= 1'b1;
					validator_data_ready <= 1'b0;
				end
				
				validator_gap_counter <= validator_gap_counter + 1;
				
				if (!validator_init) begin
					
					if (validator_gap_counter == 0) begin
						
						if (validator_counter < PACKET_LENGTH) begin
							if (!(validator_counter>7 && validator_counter <12) ) begin
								// validator_data_valid <= 1'b1;
								validator_data_ready <= 1'b1;
								validator_data <= packet[8*validator_counter +: 8];
							end
							validator_counter <= validator_counter + 1;

						end
						else if (validator_counter < PACKET_LENGTH + CRC_WIDTH/8) begin
							
							// validator_data_valid <= 1'b1;
							validator_data_ready <= 1'b1;
							validator_data <= 8'd0;
							
							validator_counter <= validator_counter + 1;

						end
						else begin
								status <= STATUS_SENDING_BACK_PACKET;
								validator_data <= 8'd0; 
								validator_data_valid <= 1'b0;
								validator_counter <=  0;
								validator_gap_counter <= 0;
								
								packet[64+:CRC_WIDTH] <= crc_out;
								/*
								packet_writing_pointer <= 64;
								packet_data_in[0+:CRC_WIDTH] <= crc_out;
								packet_we <= 1'b1;
								pw_width <= 8'd32;
								*/
								validator_init <= 1'b1;
								$display("sending back packet with crc of: %h", packet[64+:CRC_WIDTH]);
						end
					end
					else if (validator_gap_counter == VALIDATOR_GAP) begin
						validator_gap_counter <= 0;
					end
					
					
				end
				
			end
			
			STATUS_SENDING_BACK_PACKET : begin
				
				validator_init <= 1'b0;
				
				if (packet_reading_pointer>>3 < PACKET_LENGTH) begin
						if (can_write && need_to_write) begin
							byte_out[0+:8] <= packet[packet_reading_pointer+:8];
							byte_out_valid <= 1'b1;
							need_to_write <= 1'b0;
							packet_reading_pointer <= packet_reading_pointer + 8;
						end
						else begin
							need_to_write <= 1'b1;
							byte_out_valid <= 1'b0;
						end
					end
					else begin
						packet_reading_pointer <= 8;
						byte_out_valid <= 1'b0;
						need_to_write<= 1'b1;
						
						if (packet[104+:8] & LAST_PACKET_FLAG) begin 
							status <= STATUS_PENDING_TO_PACKET;
						end
						else begin
							packet_writing_pointer <= 0;
							status <= STATUS_RECEIVING_PACKET;
							packet_reading_pointer <= 0;
						end
					end
				
			end
			
		endcase
		
	end

end


endmodule
