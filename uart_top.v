`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:11:43 12/26/2017 
// Design Name: 
// Module Name:    uart_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module uart_top(

		// Clocks
		input uart_clk,
		input clk,
		
		// Resets
		input rstn,
		input uart_resetn,
		output can_write,
		output can_read,
		
		output reg output_valid,
		
		// Data to TX
		input [15:0] data_in,
		input data_valid,
		
		// Data to Rx
		output wire [15:0] data_out, 
		input read_valid,
		
		// UART peripheral interface
		output reg Tx,
		input Rx,
		
		output [9:0] monitor
    );

parameter UART_BAUD_RATE = 19200;
localparam UART_TX_GAP= 32'd5200;//CLOCK_HZ/UART_BAUD_RATE; // num of bit time gap between symbols
parameter CLOCK_HZ = 100000000;

localparam FIFO_LATENCY = 20;

reg transmit;
reg [7:0] tx_byte;
wire received;
wire [7:0] rx_byte;
wire is_receiving;
wire is_transmitting;

wire tx_wire;
always @(*) begin
	Tx <= tx_wire;
end

uart #(
		.baud_rate(19200),                // This must always be 19200
		.sys_clk_freq(CLOCK_HZ)          // The master clock frequency
	)
	u_uart(
		.clk(uart_clk),                   // The master clock for this module
		.rst(!uart_resetn),                 // Synchronous reset
		.rx(Rx),                          // Incoming serial line
		.tx(tx_wire),                          // Outgoing serial line
		.transmit(transmit),              // Signal to transmit
		.tx_byte(tx_byte),                // Byte to transmit
		.received(received),              // Indicated that a byte has been received
		.rx_byte(rx_byte),                // Byte received
		.is_receiving(is_receiving),      // Low when receive line is idle
		.is_transmitting(is_transmitting),// Low when transmit line is idle
		.recv_error(recv_error)           // Indicates error in receiving packet.
	);

///////////////////////////////////////////// UART SECTION ///////////////////////////////

///////////////////////// WRITING FIFO ///////////////////

wire wfifo_rd_clk, wfifo_wr_clk; 
assign wfifo_rd_clk = uart_clk;
assign wfifo_wr_clk = clk;
reg wfifo_clearIn;
reg wfifo_wr_en, wfifo_rd_en;
reg [15:0] wf_data_in;
wire [15:0] wf_data_out;
wire wfull, wempty;


assign can_write =  !wfull;

reg wf_output_valid;
reg [7:0] wf_output_valid_latency_counter;
wire can_read_wf = !wempty;

assign monitor[0] = wempty;
assign monitor[1] = rempty;
assign monitor[2] = is_transmitting;
assign monitor[3] = transmit;


reg wfifo_wclk_clearIn_s1;
reg wfifo_rclk_clearIn_s1, wfifo_rclk_clearIn;

always @(posedge wfifo_wr_clk , negedge rstn) begin
	if (!rstn) begin
		wfifo_wclk_clearIn_s1 <= 1'b1;
		wfifo_clearIn <= 1'b1;
	end
	else begin		
		wfifo_wclk_clearIn_s1 <= 1'b0;
		wfifo_clearIn <= wfifo_wclk_clearIn_s1;
	end
end

always @(posedge wfifo_rd_clk , negedge rstn) begin
	if (!rstn) begin
		wfifo_rclk_clearIn_s1 <= 1'b1;
		wfifo_rclk_clearIn <= 1'b1;
	end
	else begin		
		wfifo_rclk_clearIn_s1 <= 1'b0;
		wfifo_rclk_clearIn <= wfifo_rclk_clearIn_s1;
	end
end


fifo writing_fifo(

	.Q(wf_data_out),
	.EMPTY(wempty),
	.DEQ(wfifo_rd_en),
	.CLK0(wfifo_rd_clk),
	.RST0(wfifo_rclk_clearIn),
	
	.D(wf_data_in),
	.FULL(wfull),
	.ENQ(wfifo_wr_en),
	.CLK1(wfifo_wr_clk),
	.RST1(wfifo_clearIn)

);

always @(*) begin

	wfifo_wr_en <= 1'b0;
	wf_data_in <= data_in;

	if (data_valid && can_write) begin
			wfifo_wr_en <= 1'b1;
	end 
end

/*
wire transmit_done=(is_transmitting_s1==1'b1 && is_transmitting==1'b0);
reg is_transmitting_s1;

always @(posedge wfifo_wr_clk , negedge rstn) begin
  if (!rstn) is_transmitting_s1 <=1'b0;
  else is_transmitting_s1<=is_transmitting;
end
*/

reg [31:0] uart_counter_tx_delay;
wire can_send_again = (uart_counter_tx_delay >= UART_TX_GAP && !is_transmitting);

always @(posedge wfifo_wr_clk , negedge rstn) begin
	if (!rstn) begin
		uart_counter_tx_delay <= 32'd0;
	end
	else begin
		if (is_transmitting) begin
				uart_counter_tx_delay <= 32'd0;
		end
		else if (!can_send_again) begin
			uart_counter_tx_delay <= uart_counter_tx_delay + 32'd1;
		end
		else begin
			uart_counter_tx_delay <= 32'd0;
		end
	end
end

reg already_wrote;

always @(*) begin
	
	wfifo_rd_en <= 1'b0;
	transmit <= can_read_wf && !is_transmitting && can_send_again;
	tx_byte <= wf_data_out;
	
	if (transmit) begin
		wfifo_rd_en <= 1'b1;
	end

end

/*
wire fifo_rd_clk, fifo_wr_clk; 
assign fifo_rd_clk = uart_clk;
assign fifo_wr_clk = clk;
wire fifo_rst = !rstn;
reg wr_en, rd_en;
reg [15:0] wf_data_in;
wire [15:0] wf_data_out;
wire full, empty;
wire fifo_read_valid;

assign can_write = !full;

wire rd_en_wire;
assign rd_en_wire = rd_en;

fifo  writing_fifo( .rst(fifo_rst), .empty(empty), .full(full), .din(wf_data_in), .dout(wf_data_out), .rd_clk(fifo_rd_clk), .wr_clk(fifo_wr_clk), .wr_en(wr_en), .rd_en(rd_en_wire), .valid(fifo_read_valid) ); 


always @(posedge fifo_wr_clk, posedge fifo_rst) begin
	if (fifo_rst) begin
		wr_en <= 1'b0;
		wf_data_in <= 16'b0;
	end
	else begin
		wr_en <= 1'b0;
		if (!full) begin
			if (data_valid) begin
				wf_data_in <= data_in;
				wr_en <= 1'b1;
			end
		end		
	end
end

reg had_read;

always @(posedge fifo_rd_clk, negedge uart_resetn) begin
	if (!uart_resetn) begin
		rd_en <= 1'b0;
		had_read <= 1'b0;
	end
	else if (need_to_send) begin
		if (!had_read && !empty) begin
			rd_en <= 1'b1;
			had_read <= 1'b1;
		end
		else begin
			rd_en <= 1'b0;
		end
	end
	
	else begin 
		rd_en <= 1'b0;
		had_read <= 1'b0;
	end
	
	if (!uart_resetn) begin
		r_wr_en <= 1'b0;
		rf_data_in <= 1'b0;
	end
	else begin
		r_wr_en <= 1'b0;
		if (!r_full) begin
			if (received) begin
				rf_data_in <= rx_byte;
				rd_en <= 1'b1;
				r_wr_en <= 1'b1;
			end
		end		
	end
end*/

///////////////// READING FIFO /////////////////


wire rfifo_rd_clk, rfifo_wr_clk; 
assign rfifo_rd_clk = clk;
assign rfifo_wr_clk = uart_clk;
reg rfifo_clearIn;
reg rfifo_wr_en, rfifo_rd_en;
reg [15:0] rf_data_in;
wire [15:0] rf_data_out;
wire rfull, rempty;

wire can_write_rf = !rfull;
wire can_read_rf = !rempty;

reg [7:0] output_valid_latency_counter;
assign can_read = can_read_rf;

assign data_out = rf_data_out;

reg rfifo_wclk_clearIn_s1, rfifo_wclk_clearIn;
reg rfifo_rclk_clearIn_s1;

always @(posedge rfifo_wr_clk , negedge rstn) begin
	if (!rstn) begin
		rfifo_wclk_clearIn_s1 <= 1'b1;
		rfifo_wclk_clearIn <= 1'b1;
	end
	else begin		
		rfifo_wclk_clearIn_s1 <= 1'b0;
		rfifo_wclk_clearIn <= rfifo_wclk_clearIn_s1;
	end
end

always @(posedge rfifo_rd_clk , negedge rstn) begin
	if (!rstn) begin
		rfifo_rclk_clearIn_s1 <= 1'b1;
		rfifo_clearIn <= 1'b1;
	end
	else begin		
		rfifo_rclk_clearIn_s1 <= 1'b0;
		rfifo_clearIn <= rfifo_rclk_clearIn_s1;
	end
end


fifo reading_fifo(

	.Q(rf_data_out),
	.EMPTY(rempty),
	.DEQ(rfifo_rd_en),
	.CLK0(rfifo_rd_clk),
	.RST0(rfifo_clearIn),
	
	.D(rf_data_in),
	.FULL(rfull),
	.ENQ(rfifo_wr_en),
	.CLK1(rfifo_wr_clk),
	.RST1(rfifo_wclk_clearIn)

);

always @(*) begin
		if (can_write_rf && received) begin
			rfifo_wr_en <= 1'b1;
			rf_data_in[7:0] <= rx_byte;
		end
		else begin
			rfifo_wr_en <= 1'b0;
			rf_data_in <= 8'b0;
		end
end


always @(*) begin
	rfifo_rd_en <= 1'b0;
	output_valid <= 1'b0;
	
	if (can_read_rf && read_valid) begin
		rfifo_rd_en <= 1'b1;
		output_valid <= 1'b1;
	end
end

/*
wire r_fifo_rd_clk, r_fifo_wr_clk; 
assign r_fifo_rd_clk = clk;
assign r_fifo_wr_clk = uart_clk;
wire r_fifo_rst = !rstn;
reg r_wr_en, r_rd_en;
reg [15:0] rf_data_in;
wire [15:0] rf_data_out;
wire r_full, r_empty;
wire r_fifo_read_valid;

assign r_can_write = !r_full;

fifo  reading_fifo( .rst(r_fifo_rst), .empty(r_empty), .full(r_full), .din(rf_data_in), .dout(rf_data_out), .rd_clk(r_fifo_rd_clk), .wr_clk(r_fifo_wr_clk), .wr_en(r_wr_en), .rd_en(r_rd_en), .valid(r_fifo_read_valid) ); 


always @(posedge r_fifo_rd_clk, negedge uart_resetn) begin
	if (!uart_resetn) begin
		r_rd_en <= 1'b0;
		output_valid <= 1'b0;
	end
	else begin
		r_rd_en <= 1'b0;
		output_valid <= 1'b0;
		if (!r_empty) begin
			if (read_valid && !r_rd_en) begin
				r_rd_en <= 1'b1;
			end
		end
		if (r_fifo_read_valid) begin
				output_valid <= 1'b1;
				data_out <= (rf_data_out+16'd3);
		end
	end
end*/

endmodule