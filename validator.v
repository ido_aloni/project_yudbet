`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:25:45 01/21/2018 
// Design Name: 
// Module Name:    validator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module validator(
	 
	input clk,
	input resetn,
	input init, 
	input wire [LEN-1:0] mask,
	input wire [7:0] data,
	input data_valid,
	
	output wire [LEN-1:0] crc_out


);

parameter LEN = 16;

reg enable  ;
reg data_in;

crc #(.LEN(32)) checker(
	.clk(clk),
	.resetn(resetn),
	.enable(enable),
	.init(init),
	.data_in(data_in),
	.mask(mask),
	.crc_out(crc_out)
);


reg [3:0] counter;
reg in_calc;

always @(posedge clk, negedge resetn) begin
	if (!resetn) begin
		enable <= 1'b0;
		data_in <= 1'b0;
		counter <= 3'b0;
		in_calc <= 1'b0;
		
	end
	else begin
		
		if (init) begin
			enable <= 1'b0;
			data_in <= 1'b0;
			counter <= 3'b0;
			in_calc <= 1'b0;
		end
		else begin	
			enable <= 1'b0;
			if (data_valid ) begin
				in_calc <= 1;
			end
			if (in_calc) begin
				if (counter < 4'd8) begin
					data_in <= data[counter];
					counter <= counter + 3'b1;
					enable <= 1'b1;
				end
				else begin
					in_calc <= 1'b0;
					counter <= 4'b0;
				end
			end
		end
	end
end


endmodule
