`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:11:24 12/26/2017 
// Design Name: 
// Module Name:    tb_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module tb_top(
    );
	 
	 
reg clk, reset_n;

reg [5:0] debug_switches;
wire [7:0] debug_leds;

wire tx, rx; 

integer i,j,k;


////////////

localparam CRC_WIDTH=32;

wire [CRC_WIDTH-1:0] mask = 32'h04C11DB7;
reg [7:0] validator_data;
reg [7:0] validator_gap_counter;
reg validator_data_valid;
reg validation_check_done;
reg built_packet;
reg validator_init;

wire [CRC_WIDTH-1:0] crc_out;

validator #(.LEN(CRC_WIDTH)) v(
	.clk(clk),
	.resetn(reset_n),
	.data(validator_data),
	.init(validator_init),
	.mask(mask),
	.data_valid(validator_data_valid),
	.crc_out(crc_out)
);
///////////

reg [(128+40)*8:0] packet;

/*

module aes_core(
                input wire            clk,
                input wire            reset_n,

                input wire            encdec,
                input wire            init,
                input wire            next,
                output wire           ready,

                input wire [255 : 0]  key,
                input wire            keylen,

                input wire [127 : 0]  block,
                output wire [127 : 0] result,
                output wire           result_valid
               );



*/

/*
reg encdec; // 1 = enc, 0 = dec
reg aes_init; // init
reg aes_next; // command to do action
wire aes_ready; // ready to perform action
reg [127:0] key;
reg aes_keylen; // 1 256 bit keylen, 0 128 keylen
reg [127:0] aes_block;
wire [127:0] aes_result;
wire aes_res_valid;

aes_core a(
	.clk(clk),
	.reset_n(reset_n),
	
	.encdec(encdec),
	.init(aes_init),
	.next(aes_next),
	.ready(aes_ready),
	
	.key(key),
	.keylen(aes_keylen),
	
	.block(aes_block),
	.result(aes_result),
	.result_valid(aes_res_valid)
);
	
	
task wait_ready;
 begin
	while (!aes_ready)
	  begin
		 #(10);
	  end
 end
endtask // wait_ready

*/

initial begin
   

   //$dumpfile("test.vcd");
   //$dumpvars(0,tb_top);

	clk = 0;
	
	packet=0;
	validator_init = 0;

	reset_n=0;
	validator_data_valid=0;
	
	reset_n=0;
	
	#20
	
	reset_n = 1;
	
	#100
	/*
	aes_block = 128'd12345678;
	key = 128'd98765;
	aes_keylen = 0;
	encdec = 1;
	
		
	aes_init = 1;
	#10
	aes_init = 0;
	wait_ready();
	
	aes_next = 1;
	for (k=0;k<20;k=k+1)
	@(posedge clk);
	aes_next = 0;
	wait_ready();
	
	wait(aes_res_valid);
	$display("enc: %h", aes_result);
	
	#10000
	
	
	
		
	aes_block = aes_result;
	key = 128'd98765;
	aes_keylen = 0;
	encdec = 0;
	
		
	aes_init = 1;
	#10
	aes_init = 0;
	wait_ready();
	
	aes_next = 1;
	for (k=0;k<20;k=k+1)
	@(posedge clk);
	aes_next = 0;
	
	wait_ready();
	
	$display("dec: %h", aes_result);
	$display("org: %h", 128'd12345678);
	
	
	$finish;
	*/
	
	reset_n=1;
	recv_size=22;
	
	for (i=0;i<21;i=i+1) begin
		packet[(8*i)+:8] = 8'd0;
		if (i==1) 
			packet[(8*i)+:8] = 8'b10000010;
		
		if (!(i>16 && i <21)) begin
			validator_data_valid=1;
		end
		else validator_data_valid=0;
		
		validator_data	= packet[(i*8)+:8];
		
		@(posedge clk);
		validator_data_valid=0;
		
		for (j=0;j<12;j=j+1)
			@(posedge clk);
	
	end
	
	for(i=0;i<4;i=i+1) begin
		validator_data_valid=1;
		validator_data=0;
		@(posedge clk);
		validator_data_valid=0;
		
		for (j=0;j<12;j=j+1)
			@(posedge clk);
	end
	
	$display("Packet crc is tb: %h", crc_out);
	
	
	packet[136+:32] = crc_out[0+:32];
	//packet[136+:32] = 0;
	
	validator_init=1;
	
	#100
	
	validator_init=0;
	
	
	$display("Sending Header:");
	for (i=0;i<21;i=i+1) begin
		need_to_send=1;	
		
		
		byte_to_transmit=packet[8*i+:8];

		#(UART_BYTE_SEND_GAP);
		
		
	end
	
	$display("This is the response: ");

	for (i=0;i<23;i=i+1) begin
		@(posedge received);
		
		if (i<15 || i>19) begin
			validator_data_valid=1;
			validator_data=rx_byte;
			@(posedge clk);
			validator_data_valid=0;
			
			for (j=0;j<12;j=j+1)
			@(posedge clk);
		end
	end
	
	for(i=0;i<4;i=i+1) begin
		validator_data_valid=1;
		validator_data=0;
		@(posedge clk);
		validator_data_valid=0;
		
		for (j=0;j<12;j=j+1)
			@(posedge clk);
	end
	
	$display("Response crc is: %h", crc_out);
	
	//#(13000*1000)
	//@(posedge finished_recv);
	wait(finished_recv == 1);
	
	recv_size=13+128;
	
	$display("here building packets");
	
	validator_init=1;
	#10
	
	validator_init=0;
	
	
	need_to_send=0;
	
	#10
	
	//$dumpvars(0,tb_top);

	
	for (j=0;j<6;j=j+1) begin
	
		
		validator_init=1;
		#1000
		
		validator_init=0;
		#1000
		
		for (i=0;i<recv_size+1;i=i+1) begin
			
			packet[(8*i)+:8] = 0;
			if (i==0 && j<=2)
				packet[(8*i)+:8]=j;
			else if (i==0)
				packet[(8*i)+:8]=j-2;
			
			/*
			if (j==3) begin
				if (i==13)
					packet[(8*i)+:8]=8'b00000000;
				if (i==12)
					packet[(8*i)+:8]=8'd2;
				if (i>13) begin
					packet[13*8+:32] = 32'd0; // Encrypted data of the first packet
				end
			end
			else begin
				if (i==13) begin
					if (j==5) 
						packet[(8*i)+:8]=8'b00000110;
					else packet[(8*i)+:8]=8'b00000010;
				end
			end*/
					
			if (i==13) begin
				if (j==5) 
					packet[(8*i)+:8]=8'b00000110;
				else packet[(8*i)+:8]=8'b00000010;
			end

			if (!(i>7 && i <12)) begin
				validator_data_valid=1;
			end
			else validator_data_valid=0;
			
			validator_data	= packet[(i*8)+:8];
			
			@(posedge clk);
			validator_data_valid=0;
			
			for (k=0;k<12;k=k+1)
				@(posedge clk);
		
		end
		
		for(i=0;i<4;i=i+1) begin
			validator_data_valid=1;
			validator_data=0;
			@(posedge clk);
			validator_data_valid=0;
			
			for (k=0;k<12;k=k+1)
				@(posedge clk);
		end
		
		$display("Packet crc is tb: %h", crc_out);
		
		/*
		if (j==2) begin
			packet[64+:32] = 32'd666; // Some wrong crc
		end
		else begin 
			packet[64+:32] = crc_out[0+:32];
		end*/
		
		packet[64+:32] = crc_out[0+:32];
		
		$display("Sending %d packet:", j);
		for (i=0;i<recv_size+1;i=i+1) begin
			need_to_send=1;
			/*if (j==2 && i==13) 
				byte_to_transmit=8'b00000100;
			else 
				byte_to_transmit=packet[8*i+:8];*/
			byte_to_transmit=packet[8*i+:8];
			
			@(posedge sending_done_flag);
			
		end	
		need_to_send=0;
		
		
		validator_init=1;
		#10
		
		validator_init=0;
		validator_data_valid=0;
		
		$display("\nReceiving back the packet\n");
		
		for (i=0;i<recv_size+1;i=i+1) begin
			@(posedge received);
			
			if (!(i>7 && i <12)) begin
				validator_data_valid=1;
				validator_data=rx_byte;
				@(posedge clk);
				validator_data_valid=0;
				
				for (k=0;k<12;k=k+1)
				@(posedge clk);
			end
		end
		
		for(i=0;i<4;i=i+1) begin
			validator_data_valid=1;
			validator_data=0;
			@(posedge clk);
			validator_data_valid=0;
			
			for (k=0;k<12;k=k+1)
				@(posedge clk);
		end
	
		$display("This is the response");
		$display("The crc of the received packet is: %h", crc_out);
		wait( finished_recv == 1);
		
	end
	
	$finish;
	
end

always @(*) begin
	if (!core_reset_n)
		debug_switches = 6'b0;
	else 
		debug_switches = 6'b111111;
end

always begin
	#5
	clk=0;
	#5
	clk=1;
end

reg core_reset_s1;
reg core_reset_n;
always @(posedge clk, negedge reset_n) begin
	if (!reset_n) begin
		core_reset_s1 <= 1'b0;
		core_reset_n <= 1'b0;
	end
	else begin
		core_reset_s1 <= reset_n;
		core_reset_n <= core_reset_s1;
	end
end 


///////////// TOP //////////

top top_module (.UART_TX(tx), .UART_RX(rx), .SWITCH(debug_switches), .LED(debug_leds), .CLK(clk));

////////// UART ///////////


localparam UART_TX_GAP= 5300; // 5200 = 100m / 19200
localparam UART_BYTE_SEND_GAP	 = 53000	*10;

wire uart_reset;
assign uart_reset = !core_reset_n;

reg transmit;
reg [7:0] tx_byte;
wire received;
wire [7:0] rx_byte;
wire is_receiving;
wire is_transmitting;
wire recv_error; 


uart #(
	.baud_rate(19200),                // This must always be 19200
	.sys_clk_freq(100000000)          // The master clock frequency
)
uart(
	.clk(clk),                 // The master clock for this module
	.rst(uart_reset),                      // Synchronous reset
	.rx(tx),                          // Incoming serial line
	.tx(rx),                          // Outgoing serial line
	.transmit(transmit),              // Signal to transmit
	.tx_byte(tx_byte),                // Byte to transmit
	.received(received),              // Indicated that a byte has been received
	.rx_byte(rx_byte),                // Byte received
	.is_receiving(is_receiving),      // Low when receive line is idle
	.is_transmitting(is_transmitting),// Low when transmit line is idle
	.recv_error(recv_error)           // Indicates error in receiving packet.
);


reg [15:0] sending_gap_counter;
reg [31:0] sending_value_counter;
reg [31:0] receiving_value_counter;
reg [7:0] byte_to_transmit;

reg need_to_send;
reg sending_done_flag;

always @(posedge clk, posedge uart_reset) begin
	if (uart_reset) begin
		sending_gap_counter <= UART_TX_GAP;
		sending_value_counter <= 16'b0;
		transmit <= 1'b0;
		sending_done_flag<=1'd0;
	end
	else if (is_transmitting) begin
		sending_gap_counter <= 16'd0;
		transmit <= 1'b0;
		sending_done_flag <= 1'b0;
	end
	else begin	
		if (sending_gap_counter >= UART_TX_GAP) begin
			if (need_to_send) begin
				sending_value_counter <= sending_value_counter + 32'd1;
				transmit <= 1'b1;
				sending_gap_counter <= 16'd0;
				sending_done_flag <= 1'b1;
				//$display("Sending %d", byte_to_transmit);
				tx_byte <= byte_to_transmit;
				need_to_send<=0;
			end
		end
		else begin
			sending_gap_counter <= sending_gap_counter + 16'd1;
		end
	end
end

reg [15:0] recv_size;
reg finished_recv;

always @(posedge clk, posedge uart_reset) begin
	
	if (uart_reset) begin
		receiving_value_counter <= 16'd0;
		finished_recv<=0;
	end
	else if (received) begin
		
		$display("%d %d",rx_byte, receiving_value_counter);
		
		if (receiving_value_counter > recv_size || receiving_value_counter == recv_size) begin
			finished_recv <= 1;
			receiving_value_counter<=0;
		end
		else begin 
			finished_recv <= 0;
			receiving_value_counter <= receiving_value_counter+1;
		end
	end

end

endmodule
